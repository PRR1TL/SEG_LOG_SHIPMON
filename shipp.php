<?php 
    session_start();    
    if (isset($_SESSION['user'])){
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SHIPPING</title>
        <!--LIBRERIAS ESTILOS Y DISEÑOS DEL FULLCALENDAR-->
        <link href="img/wIcon.png" rel="shortcut icon" type="image/x-icon" />
        <link rel="stylesheet" href="css/fullCalendar.css"/>
        <link rel="stylesheet" href="css/fullcalendar.min.css"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.css" type="text/css" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery.js"></script>        
        
        <!--LIBRERIAS PARA EL MODAL DEL DATEPICKER-->        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <!--LIBRERIAS FULLCALENDAR--> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <?php   
            //PRIMERO VA LA SESION SI NO VA TRONAR Y VA MANDAR ERRORES                        
            include './login/mLogin.php'; 
            include './evento/mIEvento.php';
            include './evento/mAviso.php';
            
            include './extras/mUUserUser.php';
            include './extras/avisoHora.php';
            include './extras/mFinSemana.php';
            include './evento/mUEvento.php';
            include './evento/mCEvento.php';
            
            $usuario = isset($_SESSION['user']) ? $_SESSION['user']: ''; 
            
            if ($_SESSION['tipo'] == 1){
        ?>
            <script src="js/calendarSupervisor.js"></script> 
            <script>
                function valida_envia(){  
                    $('#newEvent').modal('show');
                }
            </script>
        <?php }else { ?>
            <script src="js/calendarCostumer.js"></script> 
            
            <script>
                function modificarUser(){                   
                    $('#dataUpdateUser').modal('show');
                }
                
                //VALIDACION DE MODALS PARA EL BOTON
                function valida_envia(){
                    var vToday = new Date().getDay();
                    var h = new Date().getHours();
                    if (vToday > 0 && vToday < 6 && h < 17){                    
                        $('#newEvent').modal('show');
                    }else {
                        $('#warningHora').modal('show');
                    }
                }
            </script>
        <?php }?>
        <script>      
            function salir() {
               location.href="./login/exit.php";
            }
        </script>
            
        <a align=center id="headerFixedPrincipal" class="contenedor">   
            <div class='fila0'>                
            </div>             
            <h3 class="tituloPareto" > MONITOR DE EMBARQUES</h3>        
            <div class="fila1">
                <?php if($_SESSION["tipo"] == 1) { ?>
                    <button data-target="#panelAdmin" type="button" class="btn btn-primary btn-sm pull-right glyphicon glyphicon-user" style="float: right; position: absolute; top: 5%; left: 2%; width: 9.6%;" onclick = "location='./administracion/user/adminUser.php'"
                            onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'"> Administraci&oacute;n </button>
                <?php } else if ($_SESSION["tipo"] > 1){ ?> 
                    <button data-target="#panelAdmin" type="button" class="btn btn-primary btn-sm pull-right glyphicon glyphicon-user" style="float: right; position: absolute; top: 5%; left: 2%; width: 9.6%;" onclick="modificarUser()"
                        onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'"> Usuario </button>
                <?php  } ?>
                <button type="button" data-toggle="modal" data-target="#login" class="btn btn-success btn-sm pull-right glyphicon glyphicon-log-in" style="float: left; position: absolute; top: 7%; left: 89%; width: 9.0%;" 
                        onclick="salir()" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'"> Salir</button>
            </div>                
        </a>         
    </head>
    <body>
        <div >
            <br><br>
            <!--SEPARADOR PARA DATOS DE INICIO DE SESION-->
            <div>
                <ul class="nav">
                    <li class="nav-item">                 
                        <h5 style="float: right; position: absolute; margin: 1.2% 1%;">Usuario: </h5>                 
                        <input style="float: right; position: absolute; margin: .8% 6%"  id="usuario" value="<?php echo $usuario?>" readonly="true"/>
                    </li>
                </ul>   
                <!--BOTON DE AGREGAR EVENTO-->
                <button type="button" class="btn btn-sm btn-warning btnAddUser" data-toggle="modal" data-target="#dataRegister" onclick="valida_envia()"
                        style="float: right; position: absolute; top: 55px; left: 93%;"><i class='glyphicon glyphicon-plus'></i> Agregar</button>
            </div>
            <br><br><br>
            <div id="calendar" ></div>  
        </div>        
    </body> 
</html>
<?php } else { 
        echo '<script>location.href = "index.php";</script>';
    }
?>
        

