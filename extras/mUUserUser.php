<?php 
    $usuario = $_SESSION["user"];
    
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);
    
    $sql="SELECT nombre, telefono, tipo, contrasena FROM usuarios WHERE usuario = '".$usuario."'";
    $result = sqlsrv_query($conn,$sql);
    
    while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
        $nombre = $row['nombre'];
        $telefono = $row['telefono'];
        $nTipo = $row['tipo'];
        switch ($nTipo){
            case 1:
                $tipo = "Supervisor";
                break;
            case 2:
                $tipo = "Customer Service";
                break;
        }
        $contrasena = $row['contrasena']; 
    }
?>

<form id="actualidarDatosUser">
    <div class="modal fade" id="dataUpdateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Modificar Usuario: <?php echo $usuario ?> </h4>
                </div>
                <div class="modal-body">
                    <div id="datos_ajax"></div>
                    <input type="hidden" class="form-control" id="usuario" name="usuario" value="<?php echo $usuario?>">
                    <!--</div>-->
                    <div class="form-group">
                        <label for="nombre0" class="control-label">Nombre:</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" onkeypress="return permite(event,'car')" value="<?php echo $nombre ?>" minlength="5" maxlength="48" readonly> 
                    </div>
                    <div class="form-group">
                        <label for="moneda0" class="control-label">Número Telefónico:</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" onkeypress="return permite(event,'num')"  value="<?php echo $telefono ?> " minlength="10" maxlength="10">
                    </div>
                    <div class="form-group">
                        <label for="tipo0" class="control-label">Puesto:</label> 
                        <input type ="text" class="form-control" id="tipo" name="tipo" value="<?php echo $tipo ?>" readonly >                 
                    </div>
                    <div class="form-group">
                        <label for="pass1" class="control-label">Contraseña:</label>
                        <input type="password" class="form-control" id="contrasena" name="contrasena" style="text-transform:uppercase;" minlength="4" maxlength="30" required>
                    </div>            
                    <div class="form-group">
                        <label for="pass2" class="control-label">Verificación de Contraseña:</label>
                        <input type="password" class="form-control" id="contrasena2" name="contrasena2" style="text-transform:uppercase;" minlength="4" maxlength="30" required>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>