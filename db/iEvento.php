<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    date_default_timezone_set("America/Mexico_City");
    session_start();
    $user = $_SESSION["user"];
    
    //FECHA DE REGISTRO
    $fToday = date('d/m/Y', strtotime('+0 day')); 
    //$horaToday = ;
    
    //$hToday = 
    $horaToday = explode(":", date('H:i'));
    $hToday = $horaToday[0].$horaToday[1];
    
    //echo date('H:i')," = ",$hToday;
    
    //POSTEO DE FORMULARIO 
    //APARTADO DATOS DEL EVENTO
    $fechaIniEv = $_POST["fecha"];
    
    //DESCOMPONEMOS LAS FECHAS PARA HACER LAS VALIDACIONES CORRESPONDIENTES    
    $fIni = explode("/", $fechaIniEv);    
    //FECHA INICIO
    $dI = $fIni[0]; 
    $mI = $fIni[1];     
    $yI = $fIni[2]; 
    
    $fechaI = $yI.'-'.$mI.'-'.$dI;
    
    //HORAS    
    $inicioEv = $_POST["start"];
    $finEv = $_POST["end"];
    //DESCOMPONEMOS HORAS
    $hI = explode(":", $inicioEv);
    $hF = explode(":", $finEv);
    
    $hIni = $hI[0].$hI[1];
    $hFin = $hF[0].$hF[1];  
    
    $tTotal = $hFin-$hIni;
       
    $userAsig = isset($_POST["userAsig"]) ? $_POST["userAsig"]: $_SESSION["user"];
    
    $periodoTiempo = isset( $_POST["periodoTiempo"] ) ? $_POST["periodoTiempo"] : 0; //1: MES, 2: SEMANA
    //echo "periodo: ",$periodoTiempo,"<br>";
    
    //APARTADO DE ENVIO
    $shippTo = $_POST["shipTo"];
    $nombreCliente = $_POST["nameCliente"];
    
    //DEFINIR LOS 20 DELIVERYS    
    for ($i = 1; $i < 21; $i++ ){       
        $delivery[$i] = isset($_POST["delivery".$i]) ? $_POST["delivery".$i] : 0;
        $pallets[$i] = isset($_POST["pallets".$i]) ? $_POST["pallets".$i] : 0 ;
        $pzas[$i] = isset($_POST["pzas".$i]) ? $_POST["pzas".$i] : 0 ;
    }

    $placa = $_POST["placa"];    
    $ruta = $_POST["ruta"];    
    $puerta = $_POST["dock"];   
    $chofer = $_POST["chofer"];   
    $modeTransport = $_POST["modeTransport"];
    
    //DATOS COMPLEMENTARIOS
    $nota = $_POST["nota"];
    //DATOS FIJOS
    $estado = 1;
    $tipoRegistro = 0;
    
    $color = "#828a8e"; 
    
    $cont = 0;
    
    //APARTADO DE CONEXION A BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //APARTADO PARA LAS RELACION DE BASE DE DATOS Y QUERYS NECESARIOS
    //CONSULTA PARA ASIGNAR ID
    $query = "SELECT MAX(id) as MId FROM embarques ";
    $result = sqlsrv_query($conn,$query);    
    
    while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
        $idMax = $row['MId']; //contador de registros que trae despues de la consulta        
    }
    
    $queryCliente = "SELECT nombre FROM clientes WHERE soldToParty = '$shippTo' ";
    $resultCliente = sqlsrv_query($conn,$queryCliente);    
        
    while($row = sqlsrv_fetch_array($resultCliente, SQLSRV_FETCH_ASSOC)){
        $nombreClienteConsulta = $row['nombre'] ; //contador de registros que trae despues de la consulta        
    }  
    
    //echo $nombreCliente,' & ',$nombreClienteConsulta;
    //$idPrueba = 4;
    //$idAsig = $idPrueba;
    $cont2 = 0;
    $bnToday = 0;
    $idAsig = $idMax+1;//AQUI SE LE SUMA 1 AL MAXIMO DE LOS ID'S
    
    
    //VALIDACIONES PARA EL INSERT (EMBARQUES)
    //DIAS DEL PERIODO
    //VALIDACIONES DE DIA COMBOS SELECCIONADOS   
    if ($nombreCliente == 'NO EXISTE' || !isset($nombreClienteConsulta)){
        $errors[] = "Revisar Número de Ship To";
    }else if ($fToday == $fechaIniEv && $hToday > $hIni ){
        $errors[] = "No se puede realizar el registro por Fecha y Hora";
    } else {
        if ($hIni != $hFin && $tTotal <= 200 ) {
            $x = 1;
            if ( $periodoTiempo != 0 ){
                //FECHA FINAL              
                $fechaFinEv = $_POST["fechaFin"];
                //SEPARACION Y RECALCULO DE FECHA
                $fFin = explode("/", $fechaFinEv);
                $dF = $fFin[0];
                $mF = $fFin[1];
                $yF = $fFin[2];
                $fechaFi = $yF.'-'.$mF.'-'.$dF;   
                //echo $fechaFi;
                $fechaF = strtotime($yF.'-'.$mF.'-'.$dF);

                if ($fechaFinEv < $fechaIniEv){
                    $errors []= "La fecha inicial: ".$fechaIniEv." no puede ser mayor a la final: ".$fechaFinEv;
                } else if ($hIni > $hFin ){
                    $errors []= "La hora inicial: ".$inicioEv." no puede ser mayor a la final: ".$finEv;                
                } else if ($hIni == $hFin){
                    $errors []= "La hora inicial: ".$inicioEv." no puede ser igual a la final: ".$finEv;
                }else if ($tTotal >= 201){
                    $errors []= "El embarque no puede durar mas de 2 horas";
                } else {   
                    $fechaIn = strtotime($fechaI);
                    //CALCULO DE DIAS Y VENTANAS RECURRENTES
                    $diasSelect = "";
                    for ( $i = 1; $i < 8; $i++){ //HACEMOS EL RECORRIDO DE LOS 7 COMPONTES DE LA SEMANA
                        if ( isset($_POST["d".$i]) && $_POST["d".$i] != "" ){
                            //AQUI OBTENES LOS DIAS SELECCIONADOS EN EL CALENDAR DE DIAS
                            $diaSemSelect[$x] = $_POST["d".$i];
                            $d = $diaSemSelect[$x].', ';                       

                            switch ($d){
                                case 1:
                                    $charD = 'L';
                                    break;
                                case 2:
                                    $charD = 'M';
                                    break;
                                case 3:
                                    $charD = 'Mi';
                                    break;
                                case 4:
                                    $charD = 'J';
                                    break;
                                case 5:
                                    $charD = 'V';
                                    break;
                                case 6:
                                    $charD = 'S';
                                    break;
                                case 7:
                                    $charD = 'D';
                                    break;                           
                            }
                            if ($i ==1 ){
                                $diasSelect = $diasSelect.$charD;  //AQUI SE OBTIENE UNA CADENA DE LOS DIAS ENECENDIDOS SELECCIONADOS DE LA SEMANA              
                            } else {
                                $diasSelect = $diasSelect.','.$charD;
                            }                               
                            $x += 1;  
                        }            
                    }

                    $idMaxFija = 0;
                    //CALCULO DE DIAS Y VENTANAS RECURRENTES
                    $query = "SELECT campRelacion FROM embarques WHERE id = (SELECT MAX(id) FROM embarques WHERE tipoRegistro = 2 );";
                    $result = sqlsrv_query($conn,$query);    

                    while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                        $idMaxFija = isset($row['campRelacion']) ? $row['campRelacion'] : 0 ; //contador de registros que trae despues de la consulta        
                    }                
                    $campRelacional = $idMaxFija + 1;
                    //echo $idMaxFija,' +1 ',$campRelacional;

                    for($i = $fechaIn; $i <= $fechaF; $i+=86400) {
                        //Sacar el dia de la semana con el modificador N de la funcion date                
                        $fInsert = date ("Y-m-d", $i);
                        $dia = date('N', $i);
                        $eInicio = $fInsert.' '.$inicioEv;
                        $eFin = $fInsert.' '.$finEv;   

                        //COMPRUEBA LAS PUERTAS DESOCUPADAS
                        $query = "SELECT * FROM embarques WHERE hInicio = '$hIni' AND hFin = '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                            OR hInicio > '$hIni' AND hInicio < '$hFin' AND hFin > '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                            OR hInicio < '$hIni' AND hFin > '$hIni' AND hFin < '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'";
                        $resultRegConsulta = sqlsrv_query($conn,$query);

                        while($row = sqlsrv_fetch_array($resultRegConsulta, SQLSRV_FETCH_ASSOC)){
                            $cont = $cont+1; //contador de registros que trae despues de la consulta
                        }
                        $bnIFija = 0;
                    }
                    
                    if ($cont < 1 ){//AQUI VALIDAMOS QUE NO SE TRASLAPE LA HORA
                        for($i = $fechaIn; $i <= $fechaF; $i+=86400) {
                        //Sacar el dia de la semana con el modificador N de la funcion date                
                            $fInsert = date ("Y-m-d", $i);
                            $dia = date('N', $i);
                            $eInicio = $fInsert.' '.$inicioEv;
                            $eFin = $fInsert.' '.$finEv;  
                            for ($j = 1; $j < $x; $j++ ){
                                if ($dia == $diaSemSelect[$j]){ 
                                    $queryInsert = "INSERT INTO embarques (id, eUser, fecha, hInicio, hFin, eInicio, eFin, shippTo, cliente, placas, ruta, chofer, puerta, estado, nota, userAsig, tipoRegistro, color, campRelacion, inicioReg, finReg, diaReg,modeTransport) "
                                    . "VALUES ('$idAsig', '$user', '$fInsert', '$hIni', '$hFin', '$eInicio', '$eFin', '$shippTo', '$nombreCliente', '$placa', '$ruta', '$chofer', '$puerta',' $estado', '$nota', '$userAsig', 2, '$color','$campRelacional','$fechaI','$fechaFi','$diasSelect', '$modeTransport');";
                                    $result = sqlsrv_query($conn,$queryInsert);                            

                                    if ($result){
                                        //APARTADO PARA INSERTAR LOS DELIVERYS            
                                        $queryDel = "INSERT INTO deliverys VALUES ('$idAsig', '$campRelacional', '$delivery[1]', '$delivery[2]', '$delivery[3]', '$delivery[4]', '$delivery[5]', '$delivery[6]', '$delivery[7]', '$delivery[8]', '$delivery[9]', '$delivery[10]', '$delivery[11]', '$delivery[12]', '$delivery[13]', '$delivery[14]', '$delivery[15]', '$delivery[16]', '$delivery[17]', '$delivery[18]', '$delivery[19]', '$delivery[20]');";
                                        $resultDl = sqlsrv_query($conn,$queryDel);

                                        //APARTADO PARA INSERTAR PALLETS
                                        $queryPl = "INSERT INTO pallets VALUES ('$idAsig', '$campRelacional', '$pallets[1]', '$pallets[2]', '$pallets[3]', '$pallets[4]', '$pallets[5]', '$pallets[6]', '$pallets[7]', '$pallets[8]', '$pallets[9]', '$pallets[10]', '$pallets[11]', '$pallets[12]', '$pallets[13]', '$pallets[14]', '$pallets[15]', '$pallets[16]', '$pallets[17]', '$pallets[18]', '$pallets[19]', '$pallets[20]');";
                                        $resultPl = sqlsrv_query($conn,$queryPl);

                                        //APARTADO PARA INSERTAR PZAS
                                        $queryPz = "INSERT INTO piezas VALUES ('$idAsig', '$campRelacional', '$pzas[1]', '$pzas[2]', '$pzas[3]', '$pzas[4]', '$pzas[5]', '$pzas[6]', '$pzas[7]', '$pzas[8]', '$pzas[9]', '$pzas[10]', '$pzas[11]', '$pzas[12]', '$pzas[13]', '$pzas[14]', '$pzas[15]', '$pzas[16]', '$pzas[17]', '$pzas[18]', '$pzas[19]', '$pzas[20]');";
                                        $resultPz = sqlsrv_query($conn,$queryPz);

                                        $idAsig = $idAsig+1;
                                    }else {
                                        $bnIFija = 1;
                                        break;
                                    }
                                } 
                            } 
                        }
                    } else {
                        $errors []= "Registros no realizados: Traslape de Hora-Puerta";
                    }                
                    
                    if (isset($bnIFija)) {     
                        //echo " entro en el bnIFija";
                        //SE OBTIENEN NUEVAMENTE LOS ID PARA QUE NO TRUENE
                        $query = "SELECT MAX(id) as MId FROM embarques ";
                        $resultFija = sqlsrv_query($conn,$query);    

                        while($row = sqlsrv_fetch_array($resultFija, SQLSRV_FETCH_ASSOC)){
                            $idMax = $row['MId']; //contador de registros que trae despues de la consulta        
                        }
                        $idAsig2 = $idMax+1;//AQUI SE LE SUMA 1 AL MAXIMO DE LOS ID'S

                        //echo $idAsig2;

                        if ($idAsig2 > $idAsig) {
                            //CALCULO DE DIAS Y VENTANAS RECURRENTES
                            $queryCampRel = "SELECT campRelacion FROM embarques WHERE id = (SELECT MAX(id) FROM embarques WHERE tipoRegistro = 2 );";
                            $resultIdFija = sqlsrv_query($conn, $queryCampRel);    

                            while($row = sqlsrv_fetch_array($resultIdFija, SQLSRV_FETCH_ASSOC)){
                                $idMaxFija = isset($row['MIdF']) ? $row['MIdF'] :0 ; //contador de registros que trae despues de la consulta        
                            }

                            $idMaxFija = isset($idMaxFija) ? $row['MIdF'] :0 ;
                            $campRelacional = (string) 'r'.$idMaxFija+1;

                            for($i = $fechaIn; $i <= $fechaF; $i+=86400) {
                                //Sacar el dia de la semana con el modificador N de la funcion date                
                                $fInsert = date ("Y-m-d", $i);
                                
                                //COMPRUEBA LAS PUERTAS DESOCUPADAS
                                $query = "SELECT * FROM embarques WHERE hInicio = '$hIni' AND hFin = '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                                                                        OR hInicio > '$hIni' AND hInicio < '$hFin' AND hFin > '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                                                                        OR hInicio < '$hIni' AND hFin > '$hIni' AND hFin < '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'";
                                $resultCountReg = sqlsrv_query($conn,$query);

                                while($row = sqlsrv_fetch_array($resultCountReg, SQLSRV_FETCH_ASSOC)){
                                    $cont2 = $cont+1; //contador de registros que trae despues de la consulta
                                }
                                $bnIFija = 0; 
                            }
                            
                            if ($cont2 < 1 ){//AQUI VALIDAMOS QUE NO SE TRASLAPE LA HORA
                                for($i = $fechaIn; $i <= $fechaF; $i+=86400) {
                                    $fInsert = date ("Y-m-d", $i);
                                    $dia = date('N', $i);
                                    $eInicio = $fInsert.' '.$inicioEv;
                                    $eFin = $fInsert.' '.$finEv;   
                                    for ($j = 1; $j < $x; $j++ ){
                                        if ($dia == $diaSemSelect[$j]){    
                                            //APARTADO PARA INSERTAR LOS EVENTOS

                                            $queryInsFija = "INSERT INTO embarques (id, eUser, fecha, hInicio, hFin, eInicio, eFin, shippTo, cliente, placas, ruta, chofer, puerta, estado, nota, userAsig, tipoRegistro, color, campRelacion, inicioReg, finReg, diaReg,modeTransport) "
                                            . "VALUES ('$idAsig2', '$user', '$fInsert', '$hIni', '$hFin', '$eInicio', '$eFin', '$shippTo', '$nombreCliente', '$placa', '$ruta', '$chofer', '$puerta',' $estado', '$nota', '$userAsig', 2, '$color','$campRelacional','$fechaI','$fechaFi','$diasSelect', '$modeTransport');";

                                            $result = sqlsrv_query($conn,$queryInsFija);                            

                                            if ($result){
                                                //APARTADO PARA INSERTAR LOS DELIVERYS            
                                                $queryDel = "INSERT INTO deliverys VALUES ('$idAsig2', '$campRelacional', '$delivery[1]', '$delivery[2]', '$delivery[3]', '$delivery[4]', '$delivery[5]', '$delivery[6]', '$delivery[7]', '$delivery[8]', '$delivery[9]', '$delivery[10]', '$delivery[11]', '$delivery[12]', '$delivery[13]', '$delivery[14]', '$delivery[15]', '$delivery[16]', '$delivery[17]', '$delivery[18]', '$delivery[19]', '$delivery[20]');";
                                                $resultDl = sqlsrv_query($conn,$queryDel);

                                                //APARTADO PARA INSERTAR PALLETS
                                                $queryPl = "INSERT INTO pallets VALUES ('$idAsig2', '$campRelacional', '$pallets[1]', '$pallets[2]', '$pallets[3]', '$pallets[4]', '$pallets[5]', '$pallets[6]', '$pallets[7]', '$pallets[8]', '$pallets[9]', '$pallets[10]', '$pallets[11]', '$pallets[12]', '$pallets[13]', '$pallets[14]', '$pallets[15]', '$pallets[16]', '$pallets[17]', '$pallets[18]', '$pallets[19]', '$pallets[20]');";
                                                $resultPl = sqlsrv_query($conn,$queryPl);

                                                //APARTADO PARA INSERTAR PZAS
                                                $queryPz = "INSERT INTO piezas VALUES ('$idAsig2', '$campRelacional', '$pzas[1]', '$pzas[2]', '$pzas[3]', '$pzas[4]', '$pzas[5]', '$pzas[6]', '$pzas[7]', '$pzas[8]', '$pzas[9]', '$pzas[10]', '$pzas[11]', '$pzas[12]', '$pzas[13]', '$pzas[14]', '$pzas[15]', '$pzas[16]', '$pzas[17]', '$pzas[18]', '$pzas[19]', '$pzas[20]');";
                                                $resultPz = sqlsrv_query($conn,$queryPz);
                                                $idAsig2 = $idAsig2 +1;
                                            }else {
                                                $bnIFija = 1;
                                                break;
                                            }
                                        } 
                                    }
                                }
                            } else {
                                $errors []= "Registros no realizados: Traslape de Hora-Puerta";
                            } 
                        }
                    }
                    if (isset($bnError1)){
                        $errors []= "El registro no se pudo realizar por Traslape de Hora-Puerta";
                    }
                }
            } else {   
                $fInsert = $fechaI;
                $eInicio = $fInsert.' '.$inicioEv;
                $eFin = $fInsert.' '.$finEv; 
                $campRelacional = "";

                $query = "SELECT * FROM embarques WHERE hInicio = '$hIni' AND hFin = '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                                                    OR hInicio > '$hIni' AND hInicio < '$hFin' AND hFin > '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'
                                                    OR hInicio < '$hIni' AND hFin > '$hIni' AND hFin < '$hFin' AND fecha = '$fInsert' AND puerta = '$puerta'";
                //$query = "SELECT * FROM embarques WHERE fecha = '$fInsert' AND puerta = '$puerta' AND tipoRegistro <> 0 AND hInicio = '$hIni' AND hFin = '$hFin' OR hInicio < '$hIni' AND hFin > '$hFin' OR hInicio < '$hIni' AND hFin > '$hFin' OR hInicio < '$hIni' AND hFin > '$hIni' AND hFin < '$hFin' OR hInicio < '$hFin' AND fecha = '$fInsert'";
                $result = sqlsrv_query($conn,$query);
                //echo 'fecha = ',$fInsert,' hIni: ',$hIni,' hFin: ',$hFin,'puerta = ',$puerta;
                while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                    $cont = $cont+1; //contador de registros que trae despues de la consulta
                }
                if ($cont < 1 ){//AQUI VALIDAMOS QUE NO SE TRASLAPE LA HORA
                    //AQUI EN TEORIA TENDRIA QUE MANDAR INSERT SIN PROBLEMA 
                    
                    $query = "INSERT INTO embarques (id, eUser, fecha, hInicio, hFin, eInicio, eFin, shippTo, cliente, placas, ruta, chofer, puerta, estado, nota, userAsig, tipoRegistro, color, inicioReg, finReg, modeTransport) "
                           . "VALUES ('$idAsig', '$user', '$fInsert', '$hIni', '$hFin', '$eInicio', '$eFin', '$shippTo', '$nombreCliente', '$placa', '$ruta', '$chofer', '$puerta',' $estado', '$nota', '$userAsig', 1, '$color', '$fInsert', '$fInsert','$modeTransport');";

                    //$query = "INSERT INTO embarques (id, nota) VALUES (4, 'prueba id');";
                    $result = sqlsrv_query($conn,$query);

                    if ($result){
                        //APARTADO PARA INSERTAR LOS DELIVERYS
                        $queryDel = "INSERT INTO deliverys VALUES ('$idAsig', '$campRelacional', '$delivery[1]', '$delivery[2]', '$delivery[3]', '$delivery[4]', '$delivery[5]', '$delivery[6]', '$delivery[7]', '$delivery[8]', '$delivery[9]', '$delivery[10]', '$delivery[11]', '$delivery[12]', '$delivery[13]', '$delivery[14]', '$delivery[15]', '$delivery[16]', '$delivery[17]', '$delivery[18]', '$delivery[19]', '$delivery[20]');";
                        $resultDl = sqlsrv_query($conn,$queryDel);

                        //APARTADO PARA INSERTAR PALLETS
                        $queryPl = "INSERT INTO pallets VALUES ('$idAsig', '$campRelacional', '$pallets[1]', '$pallets[2]', '$pallets[3]', '$pallets[4]', '$pallets[5]', '$pallets[6]', '$pallets[7]', '$pallets[8]', '$pallets[9]', '$pallets[10]', '$pallets[11]', '$pallets[12]', '$pallets[13]', '$pallets[14]', '$pallets[15]', '$pallets[16]', '$pallets[17]', '$pallets[18]', '$pallets[19]', '$pallets[20]');";
                        $resultPl = sqlsrv_query($conn,$queryPl);

                        //APARTADO PARA INSERTAR PZAS
                        $queryPz = "INSERT INTO piezas VALUES ('$idAsig', '$campRelacional', '$pzas[1]', '$pzas[2]', '$pzas[3]', '$pzas[4]', '$pzas[5]', '$pzas[6]', '$pzas[7]', '$pzas[8]', '$pzas[9]', '$pzas[10]', '$pzas[11]', '$pzas[12]', '$pzas[13]', '$pzas[14]', '$pzas[15]', '$pzas[16]', '$pzas[17]', '$pzas[18]', '$pzas[19]', '$pzas[20]');";
                        $resultPz = sqlsrv_query($conn,$queryPz); 
                    } else {
                        //APARTADO PARA LAS RELACION DE BASE DE DATOS Y QUERYS NECESARIOS
                        //CONSULTA PARA ASIGNAR ID
                        $query = "SELECT MAX(id) as MId FROM embarques ";
                        $result = sqlsrv_query($conn,$query);    

                        while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                            $idMax = $row['MId']; //contador de registros que trae despues de la consulta        
                        }
                        $idAsig2 = $idMax+1;//AQUI SE LE SUMA 1 AL MAXIMO DE LOS ID'S

                        if ($idAsig2 > $idAsig){
                            
                            $query = "INSERT INTO embarques (id, eUser, fecha, hInicio, hFin, eInicio, eFin, shippTo, cliente, placas, ruta, chofer, puerta, estado, nota, userAsig, tipoRegistro, color, inicioReg, finReg, modeTransport) "
                            . "VALUES ('$idAsig2', '$user', '$fInsert', '$hIni', '$hFin', '$eInicio', '$eFin', '$shippTo', '$nombreCliente', '$placa', '$ruta', '$chofer', '$puerta',' $estado', '$nota', '$userAsig', 1, '$color', '$fInsert', '$fInsert','$modeTransport');";                
                            $result = sqlsrv_query($conn,$query);

                            if ($result){
                                //APARTADO PARA INSERTAR LOS DELIVERYS
                                $queryDel = "INSERT INTO deliverys VALUES ('$idAsig2', '$campRelacional', '$delivery[1]', '$delivery[2]', '$delivery[3]', '$delivery[4]', '$delivery[5]', '$delivery[6]', '$delivery[7]', '$delivery[8]', '$delivery[9]', '$delivery[10]', '$delivery[11]', '$delivery[12]', '$delivery[13]', '$delivery[14]', '$delivery[15]', '$delivery[16]', '$delivery[17]', '$delivery[18]', '$delivery[19]', '$delivery[20]');";
                                $resultDl = sqlsrv_query($conn,$queryDel);

                                //APARTADO PARA INSERTAR PALLETS
                                $queryPl = "INSERT INTO pallets VALUES ('$idAsig2', '$campRelacional', '$pallets[1]', '$pallets[2]', '$pallets[3]', '$pallets[4]', '$pallets[5]', '$pallets[6]', '$pallets[7]', '$pallets[8]', '$pallets[9]', '$pallets[10]', '$pallets[11]', '$pallets[12]', '$pallets[13]', '$pallets[14]', '$pallets[15]', '$pallets[16]', '$pallets[17]', '$pallets[18]', '$pallets[19]', '$pallets[20]');";
                                $resultPl = sqlsrv_query($conn,$queryPl);

                                //APARTADO PARA INSERTAR PZAS
                                $queryPz = "INSERT INTO piezas VALUES ('$idAsig2', '$campRelacional', '$pzas[1]', '$pzas[2]', '$pzas[3]', '$pzas[4]', '$pzas[5]', '$pzas[6]', '$pzas[7]', '$pzas[8]', '$pzas[9]', '$pzas[10]', '$pzas[11]', '$pzas[12]', '$pzas[13]', '$pzas[14]', '$pzas[15]', '$pzas[16]', '$pzas[17]', '$pzas[18]', '$pzas[19]', '$pzas[20]');";
                                $resultPz = sqlsrv_query($conn,$queryPz);  
                            } else {
                                $errors[] = "iEvento: Line 271";
                            }                        
                        }
                    }
                }else {
                    $errors []= "Registro no realizado: Traslape de Hora-Puerta";
                }
            } 
        }else {
            if ($hIni == $hFin){
                $errors []= "La hora inicial: ".$inicioEv." no puede ser igual a la final: ".$finEv;
            }
            if ($tTotal >= 201){
                $errors []= "El embarque no puede durar mas de 2 horas";
            }        
        }
    }

    if (isset($errors)){
    ?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error;
        } ?> </strong>     
        </div>
    <?php } else {
        ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Registro guardado Correctamente</strong>     
        </div>
        <?php
        echo "<script>
            $('#newEvent').modal('hide');            
            </script>";
            echo '<script>location.href = "./shipp.php";</script>';    
    } ?>