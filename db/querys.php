<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 *  
     * ESTRUCTURA DE VARIABLES (REALIZADO POR ARELI P. CALIXTO PRR1TL)
     * $cNombreVariable es para CONSULTA
     * $iNombreVariables es para INSERT
     * $dNombreVariable es para DELETE
     * $uNombreVariables es para UPDATE
     * 
     *
 */

require_once 'conexion.php';
//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) {
    $connectionObj = new ServerConfig();
    $connectionStr = $connectionObj -> serverConnection();
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) die();
    $rawdata = array();
    $i = 0;
    while ($row = sqlsrv_fetch_array($result)) {
        $rawdata[$i] = $row;
        $i++;
    }
    $connectionObj ->serverDisconnect();
    return $rawdata;
}

function cClientes(){
    $sql = "SELECT * FROM clientes ORDER BY id ASC";
    return getArraySQL($sql);
}

function cNameClientes($soldTo){
    $sql = "SELECT nombre FROM clientes WHERE soldToParty = '$soldTo'";
    return getArraySQL($sql);
}

function cUsuarios(){
    $sql = "SELECT * FROM usuarios ORDER BY usuario;";
    return getArraySQL($sql);
}

function cUsuarioContrasena($usuario, $contrasena){
    $sql = "SELECT usuario, nombre, tipo, estado FROM usuarios WHERE usuario = '$usuario' AND contrasena = '$contrasena'";
    return getArraySQL($sql);
}

function cEdEvento ($id){
    $sql = "SELECT nombre FROM clientes WHERE id = '$id'";
    return getArraySQL($sql);
}

function uEventoTReal ($fecha, $hIni){
    $sql = "UPDATE registros SET color = '#DC1400' WHERE fecha = '$fecha' AND hIni = '$hInicio'";
    return getArraySQL($sql);
}

function uEventHInicioDia ($fecha, $hInicio){
    echo 'entra ', $hInicio;
    $sql = "UPDATE embarques SET color = '#DC1400' WHERE fecha = '$fecha' AND hIni <'$hInicio' AND estado < '3'"; 
    return getArraySQL($sql);   
}

function uEventHInicioFinDia ($fecha, $hInicio, $hFin){
    $sql = "UPDATE embarques SET color = '#DC1400' WHERE fecha = '$fecha' AND hFin = '$hFin' AND estado < '5'";
    return getArraySQL($sql);
}


