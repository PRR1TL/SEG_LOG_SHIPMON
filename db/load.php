<?php
    $server = "SGLERSQL01\sqlexpress, 1433";  
    $database = "DB_LER_SHIPMON_SQL";  
    $conn = new PDO( "sqlsrv:server=$server; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");  

    date_default_timezone_set("America/Mexico_City");
    //OBTENEMOS LA FECHA POR SEPARADO
    $fecha  = date("Y-m-d");
    $hora = date("H");
    $min = date("i");
    $hIni = $hora.$min;

    //ESTA VALIDACION ES LA QUE HACE QUE SE PINTE EL EVENTO DE ROJO CUANDO 
    //ESTA PROGRAMADO EL INICIO DEL EMBARQUE EN TIEMPO REAL
    if ($min == 0 || $min == 15 || $min == 30 || $min == 45){
        $stmt = $conn->query( "UPDATE embarques SET color = '#DC1400' WHERE fecha = '$fecha' AND hInicio = '$hIni' AND estado < '3'" ); 
        //ESTA VALIDACION ES LA QUE HACE QUE SE PINTE EL EVENTO DE ROJO CUANDO 
        //ESTA PROGRAMADO EL EMBARQUE NO HA TEMRINADO DE CARGARSE EN TIEMPO REAL
        $stmt = $conn->query( "UPDATE embarques SET color = '#DC1400' WHERE fecha = '$fecha' AND hFin = '$hIni' AND estado < '5'" ); 
    }   
    
    $stmt = $conn->query( "SELECT e.id, e.eUser, e.fecha, e.hInicio, e.hFin, e.eInicio, e.eFin, e.shippTo, e.cliente ,e.placas, e.ruta, e.chofer, e.puerta, e.estado, e.nota, e.userAsig, e.userMod, e.campRelacion, e.tipoRegistro, e.color, e.inicioReg, e.finReg, e.diaReg, e.modeTransport, e.numFac,
                                dl.dl1, dl.dl2, dl.dl3, dl.dl4, dl.dl5, dl.dl6, dl.dl7, dl.dl8, dl.dl9, dl.dl10, dl.dl11, dl.dl12, dl.dl13, dl.dl14, dl.dl15, dl.dl16, dl.dl17, dl.dl18, dl.dl19, dl.dl20,
                                pz.pz1, pz.pz2, pz.pz3, pz.pz4, pz.pz5, pz.pz6, pz.pz7, pz.pz8, pz.pz9, pz.pz10, pz.pz11, pz.pz12, pz.pz13, pz.pz14, pz.pz15, pz.pz16, pz.pz17, pz.pz18, pz.pz19, pz.pz20,
                                pl.pl1, pl.pl2, pl.pl3, pl.pl4, pl.pl5, pl.pl6, pl.pl7, pl.pl8, pl.pl9, pl.pl10, pl.pl11, pl.pl12, pl.pl13, pl.pl14, pl.pl15, pl.pl16, pl.pl17, pl.pl18, pl.pl19, pl.pl20
                         FROM embarques as e, deliverys as dl, pallets as pl, piezas as pz
                         WHERE e.id = dl.eId AND e.id = pl.eId AND e.id = dl.eID AND e.id = pz.eId AND e.tipoRegistro > 0
                         GROUP BY e.id, e.eUser, e.fecha, e.hInicio, e.hFin, e.eInicio, e.eFin, e.shippTo, e.cliente ,e.placas, e.ruta, e.chofer, e.puerta, e.estado, e.nota, e.userAsig, e.userMod, e.campRelacion, e.tipoRegistro, e.color, e.inicioReg, e.finReg, e.diaReg, e.modeTransport, e.numFac,
                                  dl.dl1, dl.dl2, dl.dl3, dl.dl4, dl.dl5, dl.dl6, dl.dl7, dl.dl8, dl.dl9, dl.dl10, dl.dl11, dl.dl12, dl.dl13, dl.dl14, dl.dl15, dl.dl16, dl.dl17, dl.dl18, dl.dl19, dl.dl20,
                                  pz.pz1, pz.pz2, pz.pz3, pz.pz4, pz.pz5, pz.pz6, pz.pz7, pz.pz8, pz.pz9, pz.pz10, pz.pz11, pz.pz12, pz.pz13, pz.pz14, pz.pz15, pz.pz16, pz.pz17, pz.pz18, pz.pz19, pz.pz20,
                                  pl.pl1, pl.pl2, pl.pl3, pl.pl4, pl.pl5, pl.pl6, pl.pl7, pl.pl8, pl.pl9, pl.pl10, pl.pl11, pl.pl12, pl.pl13, pl.pl14, pl.pl15, pl.pl16, pl.pl17, pl.pl18, pl.pl19, pl.pl20" );  
    $result = $stmt->fetchAll(PDO::FETCH_BOTH);  
    foreach($result as $row){
        $data[] = array(
            'id'=> $row["id"],                  //ID: 1,2,3...
            'usuario'=> $row["eUser"],                  //USUARIO: PRR1TL
            'fecha'=> $row["fecha"],            //FECHA: 2018-07-02
            'start'=> $row["eInicio"],          //START: 2018-07-02 00:15
            'end'=> $row["eFin"],               //START: 2018-07-02 01:15
            'title'=> $row["cliente"],          //ES EL SHIPPTO DE LA BD
            'shippTo'=> $row["shippTo"],          //ES EL SHIPPTO DE LA BD
            'cliente'=> $row["cliente"],        //NOMBRE DEL CORRESPODIENTE SHIPP TO
            'hInicio'=> $row["hInicio"],        //ES LA HORA INICIAL EN NUMERO 15
            'hFin'=> $row["hFin"],              //ES LA HORA FINAL EN NUMERO 115
            'placas'=> $row["placas"],          //PlACAS DEL TRANSPORTE EN QUE SE VA KZK-9GPF0
            'ruta'=> $row["ruta"],              //RUTA QUE TOMA EL TRANSPORTE
            'chofer'=> $row["chofer"],          //NOMBRE DEL CHOFER DE TRANSPORTE
            'puerta'=> $row["puerta"],          //PUERTA POR LA QUE SALE 1,2,3 O 4            
            'estado'=> $row["estado"],          //ESTADO EL EMBARQUE 1 AL 7:  TODO EL PROCESO            
            'nota'=> $row["nota"],              //NOTA DEL EMBARQUE
            'modeTransport'=> $row["modeTransport"],              //NOTA DEL EMBARQUE
            'userAsig'=> $row["userAsig"],      //USUARIO ASIGNADO: NOMBRE DEL USUARIO QUE PIDIO REGISTRO FUERA DE TIEMPO
            'userMod'=> $row["userMod"],        //USUARIO QUE MODIFICO ALGUN CAMPO
            'campRelacion'=> $row["campRelacion"],//SIRVE PARA LAS VENTANAS FIJAS, ES COMO UN ID Y PODER SABER DAR TRASABILIDAD
            'tipoRegistro'=> $row["tipoRegistro"],//0: OCULTA, 1: NORMAL, 2: VENTANA FIJA 
            'color'=> $row["color"],            //CODIGO DE COLOR CON EL QUE VA LA VENTANA DE ACUERDO AL ESTADO
            'inicioReg'=> $row["inicioReg"],            //CODIGO DE COLOR CON EL QUE VA LA VENTANA DE ACUERDO AL ESTADO
            'finReg'=> $row["finReg"],            //CODIGO DE COLOR CON EL QUE VA LA VENTANA DE ACUERDO AL ESTADO
            'diasReg'=> $row["diaReg"],            //CODIGO DE COLOR CON EL QUE VA LA VENTANA DE ACUERDO AL ESTADO    
            'numFac'=> $row["numFac"],            //CODIGO DE COLOR CON EL QUE VA LA VENTANA DE ACUERDO AL ESTADO    
            //AQUI INICIAN LOS DELIVERYS            
            'dl1'=> $row["dl1"],            
            'dl2'=> $row["dl2"],            
            'dl3'=> $row["dl3"],            
            'dl4'=> $row["dl4"],            
            'dl5'=> $row["dl5"],            
            'dl6'=> $row["dl6"],            
            'dl7'=> $row["dl7"],            
            'dl8'=> $row["dl8"],            
            'dl9'=> $row["dl9"],            
            'dl10'=> $row["dl10"],    
            'dl11'=> $row["dl11"],            
            'dl12'=> $row["dl12"],            
            'dl13'=> $row["dl13"],            
            'dl14'=> $row["dl14"],            
            'dl15'=> $row["dl15"],            
            'dl16'=> $row["dl16"],            
            'dl17'=> $row["dl17"],            
            'dl18'=> $row["dl18"],            
            'dl19'=> $row["dl19"],            
            'dl20'=> $row["dl20"],  
            //AQUI INICIAN LOS PALLETS
            'pl1'=> $row["pl1"],            
            'pl2'=> $row["pl2"],            
            'pl3'=> $row["pl3"],            
            'pl4'=> $row["pl4"],            
            'pl5'=> $row["pl5"],            
            'pl6'=> $row["pl6"],            
            'pl7'=> $row["pl7"],            
            'pl8'=> $row["pl8"],            
            'pl9'=> $row["pl9"],            
            'pl10'=> $row["pl10"],    
            'pl11'=> $row["pl11"],            
            'pl12'=> $row["pl12"],            
            'pl13'=> $row["pl13"],            
            'pl14'=> $row["pl14"],            
            'pl15'=> $row["pl15"],            
            'pl16'=> $row["pl16"],            
            'pl17'=> $row["pl17"],            
            'pl18'=> $row["pl18"],            
            'pl19'=> $row["pl19"],            
            'pl20'=> $row["pl20"], 
            //AQUI INICIAN LAS PIEZAS
            'pz1'=> $row["pz1"],            
            'pz2'=> $row["pz2"],            
            'pz3'=> $row["pz3"],            
            'pz4'=> $row["pz4"],            
            'pz5'=> $row["pz5"],            
            'pz6'=> $row["pz6"],            
            'pz7'=> $row["pz7"],            
            'pz8'=> $row["pz8"],            
            'pz9'=> $row["pz9"],            
            'pz10'=> $row["pz10"],    
            'pz11'=> $row["pz11"],            
            'pz12'=> $row["pz12"],            
            'pz13'=> $row["pz13"],            
            'pz14'=> $row["pz14"],            
            'pz15'=> $row["pz15"],            
            'pz16'=> $row["pz16"],            
            'pz17'=> $row["pz17"],            
            'pz18'=> $row["pz18"],            
            'pz19'=> $row["pz19"],            
            'pz20'=> $row["pz20"]
        );
    }

    echo json_encode($data);
?>
