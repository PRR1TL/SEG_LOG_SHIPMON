<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *         CREADO POR : ARELI P. CALIXTO
 *  FECHA DE CREACION : 20/06/2018
 *        DESCRIPCION : MODAL SOLO PARA VISUALIZACION DE EVENTO 
 */
?>

<html>
    <head></head>
    <body>    
        <form id="verEvento" method="post">
            <div id="selectEvent" class="modal fade" tabindex="-1" role="dialog">       
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 align=center class="modal-title">Visualizar</h4>
                        </div>
                        <div class="modal-body"> 
                            <div id="datos_ajaxCons"></div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Evento</h3>
                                </div>
                                <!--style="border-style: double; border-width: 1px;"-->
                                <div class="panel-body"> 
                                    <div class="row">
                                        <div class="col-md-4" style="margin-left: -5px;" >
                                            <span name="dia" align="center" ><b> Día: </b></span>
                                            <input type="text" name="fechaICons" id="fechaICons" class="input" value="" style=" margin-top: -20px; margin-left: -3px; width: 47%" readonly>   
                                            
                                            <input type="text" name="fechaFCons" id="fechaFCons" class="input" value="" style=" margin-top: -30px; margin-left: 118px; width: 47%" readonly>
                                        </div>
                                        <div class="col-md-4"  >
                                            <span name="fechaCons" align="center" style="margin-left: 22px;" > <b> Hora: </b></span>                                        
                                            <input name="inicioCons" id="inicioCons" style="width: 25%;" readonly> 
                                            <span><b> a </b></span>
                                            <input name="finCons" id="finCons"  style=" width: 25%;" readonly>
                                        </div> 
                                        <div class="col-md-4" >
                                            <span name="txtUsuarioDes" align="center" ><b>Usuario: </b></span>  
                                            <input name="userAsigCons" id="userAsigCons" style="width: 60%;" readonly>
                                        </div> 
                                    </div>
                                    <div class="row" style=" margin-top: 7px;">                                       
                                        <div class="col-md-3" id="diasFijos" name="diasFijo" >
                                            <span name="txtDias" align="center" style="margin-left: -5px;" > <b> Días: </b></span>
                                            <input name="diaSelectCons" style="width: 86%; margin-top: -28px; margin-left: 30px" id="diaSelectCons" readonly>
                                        </div>
                                        
                                        <div class="col-md-5" id="divEstado" name="divEstado">
                                            <span name="txtEstado" align="center"  ><b>Estado: </b></span>                                        
                                            <input name="estadoCons" id="estadoCons" style="width: 72%;" readonly> 
                                        </div>
                                        
                                        <div class="col-md-3" id="divFac" name="divFac" style=" margin-left: -5px;" >
                                            <span name="txtFac" align="center"><b>Factura: </b></span>                                        
                                            <input name="facturaCons" id="facturaCons" style="width: 90%; margin-top: -28px; margin-left: 58px" readonly> 
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="panel panel-default" style="margin-top: -10px">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Envio</h3>
                                </div>
                                <div class="panel-body">      
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Sold to  : </b></span>
                                            <input name="soldCons" id="soldCons" style="width: 55%;" readonly>
                                        </div>
                                        <div class="col-md-8" >
                                            <input style="width: 95%" name="nombreClienteCons" id="nombreClienteCons" readonly />
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-4" >
                                            <span><b> Delivery: </b></span> 
                                            <input id="delivery1Cons" name="delivery1Cons" maxlength="10" style="width: 55%;" placeholder="1234567890" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span><b> Pallets: </b></span>
                                            <input id="pallets1Cons" name="pallets1Cons" maxlength="3" id="palletsCons" style="width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span style="margin-left: 10px;"><b> Piezas: </b></span>
                                            <input id="pzas1Cons" name="pzas1Cons" maxlength="4" id="cantidadCons" style="width: 50%;" placeholder="1234" readonly/>
                                        </div>
                                    </div> 
                                    <!--AQUI INICIA EL APARTADO DE DIVS OCULTOS-->
                                    <!--(2)   -->
                                    <div class="row no" id="componentes2Cons" >
                                        <div class="col-md-4" >
                                            <span ><b> Delivery: </b></span> 
                                            <input id="delivery2Cons" name="delivery2Cons" maxlength="10" style="width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets2Cons" name="pallets2Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                           <input id="pzas2Cons" name="pzas2Cons" maxlength="4" style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(3)-->
                                    <div class="row no" id="componentes3Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery3Cons" name="delivery3Cons" maxlength="10" style="width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets3Cons" name="pallets3Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                           <input id="pzas3Cons" name="pzas3Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(4)-->
                                    <div class="row no" id="componentes4Cons">
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery4Cons" name="delivery4Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets4Cons" name="pallets4Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas4Cons" name="pzas4Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(5)-->
                                    <div class="row no" id="componentes5Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery5Cons" name="delivery5Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4">
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets5Cons" name="pallets5Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4">
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas5Cons" name="pzas5Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(6)-->
                                    <div class="row no" id="componentes6Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery6Cons" name="delivery6Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4">
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets6Cons" name="pallets6Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas6Cons" name="pzas6Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(7)-->
                                    <div class="row no" id="componentes7Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>    
                                            <input id="delivery7Cons" name="delivery7Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets7Cons" name="pallets7Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas7Cons" name="pzas7Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(8)-->
                                    <div class="row no" id="componentes8Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery8Cons" name="delivery8Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets8Cons" name="pallets8Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas8Cons" name="pzas8Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(9)-->
                                    <div class="row no" id="componentes9Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery9Cons" name="delivery9Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets9Cons" name="pallets9Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas9Cons" name="pzas9Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(10)-->
                                    <div class="row no" id="componentes10Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>    
                                            <input id="delivery10Cons" name="delivery10Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets10Cons" name="pallets10Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas10Cons" name="pzas10Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly />
                                        </div>                                        
                                    </div>
                                    <!--(11)-->
                                    <div class="row no" id="componentes11Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>    
                                            <input id="delivery11Cons" name="delivery11Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets11Cons" name="pallets11Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4">
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas11Cons" name="pzas11Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(12)-->
                                    <div class="row no" id="componentes12Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery12Cons" name="delivery12Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets12Cons" name="pallets12Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas12Cons" name="pzas12Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(13)-->
                                    <div class="row no" id="componentes13Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery13Cons" name="delivery13Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets13Cons" name="pallets13Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas13Cons" name="pzas13Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(14)-->
                                    <div class="row no" id="componentes14Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>    
                                            <input id="delivery14Cons" name="delivery14Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets14Cons" name="pallets14Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas14Cons" name="pzas14Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(15)-->
                                    <div class="row no" id="componentes15Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery15Cons" name="delivery15Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets15Cons" name="pallets15Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4">
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas15Cons" name="pzas15Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(16)-->
                                    <div class="row no" id="componentes16Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery16Cons" name="delivery16Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets16Cons" name="pallets16Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas16Cons" name="pzas16Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(17)-->
                                    <div class="row no" id="componentes17Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery17Cons" name="delivery17Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets17Cons" name="pallets17Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas17Cons" name="pzas17Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(18)-->
                                    <div class="row no" id="componentes18Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery18Cons" name="delivery18Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets18Cons" name="pallets18Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas18Cons" name="pzas18Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(19)-->
                                    <div class="row no" id="componentes19Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery19Cons" name="delivery19Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets19Cons" name="pallets19Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas19Cons" name="pzas19Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>
                                    <!--(20)-->
                                    <div class="row no" id="componentes20Cons" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>    
                                            <input id="delivery20Cons" name="delivery20Cons" maxlength="10" style=" width: 55%;" placeholder="0123456789" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets20Cons" name="pallets20Cons" maxlength="3" style=" width: 50%;" placeholder="123" readonly/>
                                        </div>
                                        <div class="col-md-4" >
                                           <span style="margin-left: 10px;"><b > Piezas: </b></span>
                                            <input id="pzas20Cons" name="pzas20Cons" maxlength="4"  style="width: 50%;" placeholder="1234" readonly/>
                                        </div>                                        
                                    </div>                                    
                                </div> 
                            </div>                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos de Transporte</h4>
                                </div>
                                <div class="panel-body"> 
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Placa: </b></span>
                                            <input id="placaCons" name="placaCons" style="width: 68%;" maxlength="12" placeholder="Placa" type="text" readonly />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b>Ruta: </b></span>
                                            <input id="rutaCons" name="rutaCons" style="width: 78%;" maxlength="50" placeholder="Ruta" type="text" readonly />
                                        </div>
                                        <div class="col-md-3" >
                                            <select id="dockCons" name="dockCons" style="width: 100%; " disabled="true" readonly>
                                                <option value="" selected="false"  disabled="true" >Dock</option>
                                                <option value="1" >Dock 1</option>
                                                <option value="2" >Dock 2</option>
                                                <option value="3" >Dock 3</option>
                                                <option value="4" >Dock 4</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-7" >
                                            <span><b> Chofer: </b></span> 
                                            <input id="choferCons" name="choferCons" style="width: 80%;" maxlength="50" placeholder="Chofer" type="text" onkeypress="return permite(event,'car')" readonly />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b> Mode Transport: </b></span> 
                                            <select id="modeTransportCons" name="modeTransportCons" style="float: right; width: 42%; " disabled="true" readonly>
                                                <option value="0" selected="false" disabled="true" >Seleccion </option>
                                                <option value="1" >Aereo</option>
                                                <option value="2" >Maritimo</option>
                                                <option value="3" >Terrestre</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>  
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos Complementarios</h4>
                                </div>
                                <div class="panel-body"> 
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <span><b>Nota: </b></span>
                                            <textarea id="notaCons" name="notaCons" style="margin-top: -25px; margin-left: 40px; height: 20px; width: 90%"  type="text" readonly > </textarea>                                        
                                        </div> 
                                    </div>
                                </div>  
                            </div>  
                         </div >
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->         
            </div>          
        </form>
    </body>  
</html>

