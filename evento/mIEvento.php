<head>    
    <?php 
        //CONFIGURACION DE FECHAS DE LOS PICKERS
        if ($_SESSION['tipo'] == 1){
            $fInicio = date('d/m/Y', strtotime('+0 day')); 
            $fLimite = date('d/m/Y', strtotime('+1 day'));
            $fLimiteFin = date('d/m/Y', strtotime('+3 year')); 
        }else{
            $fInicio = date('d/m/Y', strtotime('+1 day')); 
            $fLimite = date('d/m/Y', strtotime('+365 day')); 
            $fLimiteFin = date('d/m/Y', strtotime('+1 year')); 
        }        
        
        //AQUI SE OBTIENEN TODAS LAS HORAS POR RANGOS DE 15 MIN
        for ($i = 0; $i < 24; $i++){    
            if ($i < 10){
                $i = '0'.$i;
            }
            if ($i == 0){
                $x = 0;
            } else {
                $x = $i * 4;
            } 
            for ($j = 0; $j < 60; $j+=15 ){
                if ($j < 10){
                    $horas[$x] = $i.':0'.$j;
                } else{
                    $horas[$x] = $i.':'.$j;
                }
                $x+=1;
            }
        }        
        
        //CONEXION A BD        
        $server = "SGLERSQL01\sqlexpress, 1433";  
        $database = "DB_LER_SHIPMON_SQL";  
        $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");
        
    ?>
    
    <style>
        .no {
            display:none;
        }
        .si {
            display:block;
        }
    </style>
    
    <script type="text/javascript">            
        
        //DATE PICKER
        $( function() {
            $( "#fecha" ).datepicker({
                minDate: <?php echo "'$fInicio'" ?> , 
                maxDate: <?php echo "'$fLimite'" ?>,
                dateFormat: 'dd/mm/yy'
            });
        } );
        
        $( function() {
            $( "#fechaFin" ).datepicker({
                minDate: <?php echo "'$fInicio'" ?> , 
                maxDate: <?php echo "'$fLimiteFin'" ?>,
                dateFormat: 'dd/mm/yy'
            });
        } );
        
        //FUNCION PARA CUANDO SE HACE EL CAMBIO DE SOLD TO PARTY
        //AQUI SE LLENA EL INPUT DEL NOMBRE DEL CLIENTE (SOLD TO PARTY)
        function cambioOpcionesIEvento(){ 
            var id = document.getElementById('soldTo').value;
            var dataString = 'action='+ id;

            $.ajax({
                url: './db/getClienteName.php',
                data: dataString,
                cache: false,
                success: function(r){
                    $("#showId").html(r);
//                        alert('Termino');
                } 
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
              });                        
        }
        
        var cST = 0;
        function keyShipTo(e,id){ 
            
            (e.keyCode) ? ev = e.keyCode: ev = e.which;
            console.log('K: '+ev);
            // Si la tecla pulsada es enter (codigo ascii 13)
            if( ev == 13 || ev == 9 ){
                // Si la variable id contiene "submit" enviamos el formulario
                if(id=="submit"){
                    document.forms[0].submit();
                }else{
                    // nos posicionamos en el siguiente input
                    document.getElementById(id).focus();
                }
            } else {
                var idShip = document.getElementById('shipTo').value;
                console.log(idShip.length);
                if (idShip.length == 10){ 
                    var dataString = 'action='+ idShip;
                    $.ajax({
                        url: './db/getClienteName.php',
                        data: dataString,
                        cache: false,
                        success: function(r){
                            $("#showId").html(r);
                        } 
                    }).fail( function( jqXHR, textStatus, errorThrown ) {
                        if (jqXHR.status === 0) {
                            alert('Not connect: Verify Network.');
                        } else if (jqXHR.status == 404) {
                            alert('Requested page not found [404]');
                        } else if (jqXHR.status == 500) {
                            alert('Internal Server Error [500].');
                        } else if (textStatus === 'parsererror') {
                            alert('Requested JSON parse failed.');
                        } else if (textStatus === 'timeout') {
                            alert('Time out error.');
                        } else if (textStatus === 'abort') {
                            alert('Ajax request aborted.');
                        } else {
                            alert('Uncaught Error: ' + jqXHR.responseText);
                        }
                    }); 
                } else {
                    document.getElementById('nameCliente').value = "";
                }
            }
        }
        
        function permite(elEvento, permitidos) {
            // Variables que definen los caracteres permitidos
            var numeros = "0123456789";
            var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            var numeros_caracteres = numeros + caracteres;
            var teclas_especiales = [8, 37, 39, 46];
            // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
            // Seleccionar los caracteres a partir del parámetro de la función
            switch(permitidos) {
              case 'num':
                permitidos = numeros;
                break;
              case 'car':
                permitidos = caracteres;
                break;
              case 'num_car':
                permitidos = numeros_caracteres;
                break;
            }
            // Obtener la tecla pulsada 
            var evento = elEvento || window.event;
            var codigoCaracter = evento.charCode || evento.keyCode;
            var caracter = String.fromCharCode(codigoCaracter);

            // Comprobar si la tecla pulsada es alguna de las teclas especiales
            // (teclas de borrado y flechas horizontales)
            var tecla_especial = false;
            for(var i in teclas_especiales) {
                if(codigoCaracter == teclas_especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }
            // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
            // o si es una tecla especial
            return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }
        
        a = 0;
        function addDeliverys(){
            a++;
            console.log("a: "+a);
            if (a <= 20){
                switch(a){
                    case 1:
                        componentes2.className='si';
                        break;
                    case 2:
                        componentes3.className='si';
                        break; 
                    case 3:
                        componentes4.className='si';
                        break;
                    case 4:
                        componentes5.className='si';
                        break;
                    case 5:
                        componentes6.className='si';
                        break;
                    case 6:
                        componentes7.className='si';
                        break;
                    case 7:
                        componentes8.className='si';
                        break;
                    case 8:
                        componentes9.className='si';
                        break;
                    case 9:
                        componentes10.className='si';
                        break;
                    case 10:
                        componentes11.className='si';
                        break;
                    case 11:
                        componentes12.className='si';
                        break;
                    case 12:
                        componentes13.className='si';
                        break; 
                    case 13:
                        componentes14.className='si';
                        break;
                    case 14:
                        componentes15.className='si';
                        break;
                    case 15:
                        componentes16.className='si';
                        break;
                    case 16:
                        componentes17.className='si';
                        break;
                    case 17:
                        componentes18.className='si';
                        break;
                    case 18:
                        componentes19.className='si';
                        break;
                    case 19:
                        componentes20.className='si';
                        break;
                    default :
                        a = 19;
                        break;
                }
            } else {
                a = 19;
            }
        }
        
        function deleteBotones(){
            switch(a){
                case 1:
                    componentes2.className='no';
                    document.getElementById("delivery2").value = "";
                    document.getElementById("pallets2").value = "";
                    document.getElementById("pzas2").value = "";
                    a--;
                    break;
                case 2:
                    componentes3.className='no';
                    document.getElementById("delivery3").value = "";
                    document.getElementById("pallets3").value = "";
                    document.getElementById("pzas3").value = "";
                    a--;
                    break; 
                case 3:
                    componentes4.className='no';
                    document.getElementById("delivery4").value = "";
                    document.getElementById("pallets4").value = "";
                    document.getElementById("pzas4").value = "";
                    a--;
                    break;
                case 4:
                    componentes5.className='no';
                    document.getElementById("delivery5").value = "";
                    document.getElementById("pallets5").value = "";
                    document.getElementById("pzas5").value = "";
                    a--;
                    break;
                case 5:
                    componentes6.className='no';
                    document.getElementById("delivery6").value = "";
                    document.getElementById("pallets6").value = "";
                    document.getElementById("pzas6").value = "";
                    a--;
                    break;
                case 6:
                    componentes7.className='no';
                    document.getElementById("delivery7").value = "";
                    document.getElementById("pallets7").value = "";
                    document.getElementById("pzas7").value = "";
                    a--;
                    break;
                case 7:
                    componentes8.className='no';
                    document.getElementById("delivery8").value = "";
                    document.getElementById("pallets8").value = "";
                    document.getElementById("pzas8").value = "";
                    a--;
                    break;
                case 8:
                    componentes9.className='no';
                    document.getElementById("delivery9").value = "";
                    document.getElementById("pallets9").value = "";
                    document.getElementById("pzas9").value = "";
                    a--;
                    break;
                case 9:
                    componentes10.className='no';
                    document.getElementById("delivery10").value = "";
                    document.getElementById("pallets10").value = "";
                    document.getElementById("pzas10").value = "";
                    a--;
                    break;
                case 10:
                    componentes11.className='no';
                    document.getElementById("delivery11").value = "";
                    document.getElementById("pallets11").value = "";
                    document.getElementById("pzas11").value = "";
                    a--;
                    break;
                case 11:
                    componentes12.className='no';
                    document.getElementById("delivery12").value = "";
                    document.getElementById("pallets12").value = "";
                    document.getElementById("pzas12").value = "";
                    a--;
                    break;
                case 12:
                    componentes13.className='no';
                    document.getElementById("delivery13").value = "";
                    document.getElementById("pallets13").value = "";
                    document.getElementById("pzas13").value = "";
                    a--;
                    break; 
                case 13:
                    componentes14.className='no';
                    document.getElementById("delivery14").value = "";
                    document.getElementById("pallets14").value = "";
                    document.getElementById("pzas14").value = "";
                    a--;
                    break;
                case 14:
                    componentes15.className='no';
                    document.getElementById("delivery15").value = "";
                    document.getElementById("pallets15").value = "";
                    document.getElementById("pzas15").value = "";
                    a--;
                    break;
                case 15:
                    componentes16.className='no';
                    document.getElementById("delivery16").value = "";
                    document.getElementById("pallets16").value = "";
                    document.getElementById("pzas16").value = "";
                    a--;
                    break;
                case 16:
                    componentes17.className='no';
                    document.getElementById("delivery17").value = "";
                    document.getElementById("pallets17").value = "";
                    document.getElementById("pzas17").value = "";
                    a--;
                    break;
                case 17:
                    componentes18.className='no';
                    document.getElementById("delivery18").value = "";
                    document.getElementById("pallets18").value = "";
                    document.getElementById("pzas18").value = "";
                    a--;
                    break;
                case 18:                        
                    componentes19.className='no';
                    document.getElementById("delivery19").value = "";
                    document.getElementById("pallets19").value = "";
                    document.getElementById("pzas19").value = "";
                    a--;
                    break;
                case 19:
                    componentes20.className='no';
                    document.getElementById("delivery20").value = "";
                    document.getElementById("pallets20").value = "";
                    document.getElementById("pzas20").value = "";
                    a--;
                    break;
            }
        }
        
//        VALIDACION PARA LOS RADIO BUTOM DE FRECUENCIA (M: MES / S:SEMANA)
        var era;
        var previo=null;

        function uncheckRadioIEven(rbutton){
            ////console.log(rbutton);
            if(previo && previo != rbutton){
                previo.era = false;
            }
            if(rbutton.checked == true && rbutton.era == true){
                rbutton.checked = false;
            }
            
            if (rbutton.checked == true ){
                //AQUI HACEMOS VISIBLE EL DIV DE CANTIDAD Y DIAS
                datPeriodo.className='si';
            }else {
                datPeriodo.className='no';
            }
            
            rbutton.era = rbutton.checked;
            previo=rbutton;
        }
        
        //CAMBIAR DE COMPONENTE (FUNCIONA PARA  TODOS, AHORA SOLO SE APLICA PARA INPUTS RESTRINGIDOS A NUMEROS)
        
        function saltar(e,id){
            // Obtenemos la tecla pulsada
            (e.keyCode) ? k = e.keyCode: k = e.which;
            //console.log('K: '+k);
            // Si la tecla pulsada es enter (codigo ascii 13)
            if(k == 13 || k == 9){
                // Si la variable id contiene "submit" enviamos el formulario
                if(id=="submit"){
                    document.forms[0].submit();
                }else{
                    // nos posicionamos en el siguiente input
                    document.getElementById(id).focus();
                }
            }
        }
        
        
    </script>
</head>

<body>
    <?php 
        $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo']:0 ;
        $dia = 26;
        $hInicio = 15;
        $hFin = 18;
    ?>
    
        <form id="nuevoEvento" method="post">
            <div id="newEvent" class="modal fade" tabindex="-1" role="dialog">       
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background:#a9dfbf;" > 
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 align=center class="modal-title">NUEVO REGISTRO</h4>
                        </div>
                        <div class="modal-body"> 
                            <div id="datos_ajax_event"></div>  
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Evento</h3>
                                </div>
                                <div class="panel-body"> 
                                    <div class="row">                                        
                                        <div class="col-md-3" style="margin-left: -5px; " >
                                            <span name="dia" align="center" ><b> Día: </b></span>
                                            <input type="text" name="fecha" maxlength="10" id="fecha" placeholder="dd/mm/yyyy" class="form-control input" value="" style="margin-top: -28px; margin-left: 30px; width: 90%" required="">
                                            <!-- <input id="fecha" name="fecha" type="text" style="width: 70%;" value="<?php echo "$fInicio" ?>"  > -->
                                        </div>
                                        <div class="col-md-5" style="margin-left: -5px; ">
                                            <span name="fecha" align="center" > <b> Hora: </b></span>                                        
                                            <select class="cmbFecha" name="start" id="start" placeholder="Inicio" style="width: 35%">
                                                <?php 
                                                    for ($x = 0; $x < 96; $x++){ 
                                                        echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                       
                                                    } 
                                                ?>     
                                                <!--                                    aqui se tiene que hacer un contador para los valores-->
                                            </select>                               

                                            <span><b> a </b></span>
                                            <select class="cmbFecha" name="end" id="end" placeholder="Inicio" style="width: 35%">
                                                <?php 
                                                    for ($x = 0; $x < 96; $x++){ 
                                                        echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                       
                                                    } 
                                                ?> 
                                            </select>
                                        </div>
                                        <?php if($tipo == 1){ ?>                     
                                            <div class="col-md-2">
                                                <select id="userAsig" name="userAsig" style=" float: right; width: 125%" required>
                                                    <option value='0' selected disabled="disabled"> Usuario </option>
                                                    <?PHP 
                                                        if ( $conn ){
                                                            $stmt = $conn->query( "SELECT usuario FROM usuarios WHERE nombre NOT LIKE ('%DESARROLLO%')" );  
                                                            $result = sqlsrv_query($conn,$stmt);  

                                                            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                                extract($row);
                                                                echo "<option value='".$usuario."'>" . $usuario. "</option>";
                                                            }
                                                            $result = sqlsrv_close($conn);
                                                        } else {
                                                            echo "<option value='0' selected> ERROR: 218 mIEvento </option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>                                            
                                        <?php } ?>
                                        <div class="col-md-2"> 
                                            <label class="form-check-label">
                                                Fija
                                                <input class="form-check-input" type="radio" name="periodoTiempo" id="periodoTiempo" onclick="uncheckRadioIEven(this)" value="1"> 
                                            </label>                                            
                                        </div>
                                    </div>                                    
                                    <div class="row no" id="datPeriodo" style="margin-top: 5px" >
                                        <div class="col-md-3" style="margin-top: 5px; margin-left: -18px" >
                                            <span name="dia" align="center" ><b> Fin: </b></span>
                                            <input type="text"  maxlength="10" name="fechaFin" id="fechaFin" placeholder="dd/mm/yyyy" class="form-control input" value="" style="margin-top: -28px; margin-left: 30px; width: 95%" >
                                        </div>
                                        <div class="col-md-8" style="margin-top: 5px">
                                            
                                            <label class="form-check-label" style="margin-left: 5px">
                                                <input class="form-check-input" type="checkbox" name="d7" id="d7" value="7"> D
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">                                                
                                                <input class="form-check-input" type="checkbox" name="d1" id="d1" value="1"> L
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d2" id="d2" value="2"> M
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d3" id="d3" value="3"> Mi
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d4" id="d4" value="4"> J
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d5" id="d5" value="5"> V
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d6" id="d6" value="6"> S
                                            </label>       
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="panel panel-default" style="margin-top: -10px">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Envio</h3>
                                </div>
                                <div class="panel-body">      
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Ship to : </b></span>                                            
                                            <input id="shipTo" name="shipTo" maxlength="10" minlength="10" style="margin-right: 5px; float: right; width: 60%; display: block;" onkeypress="return permite(event,'num')" onkeyup="keyShipTo(event,'delivery1')" />
                                        </div>
                                        
                                        <div class="col-md-8" id="showId" >
                                            <input style="float: right; width: 100%" name="nameCliente" id="nameCliente" readonly/>
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px" >
                                        <div class="col-md-4" >
                                            <span style="margin-left: -3px;" ><b> Delivery: </b></span>  
                                            <input id="delivery1" name="delivery1" minlength="10" maxlength="10" style="margin-left: -3px; width: 62%;" onkeypress="return permite(event,'num')" placeholder="0123456789" onkeyup="saltar(event,'pallets1')" type="text"  
                                                   equired />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets1" name="pallets1" maxlength="3"  style="float: right; width: 54%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas1')" required />
                                        </div> 
                                        <div class="col-md-3" >
                                            <span ><b > Piezas: </b></span>
                                            <input id="pzas1" name="pzas1" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery2')" required />
                                        </div>
                                        
                                        <div class="col-md-2" align="center" >
                                            <input type="button" class="btn btn-xs btn-warning" id="add_cancion" onClick="addDeliverys()" value="+" required />
                                            <input type="button" class="btn btn-xs btn-danger" id="delet_cancion" onClick="deleteBotones()" value=" - " required />
                                        </div>
                                    </div> 
                                    <!--AQUI INICIA EL APARTADO DE DIVS OCULTOS-->
                                    <!--(2)   -->
                                    <div class="row no" id="componentes2" >
                                        <div class="col-md-4" >
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span> 
                                            <input id="delivery2" name="delivery2" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')" placeholder="0123456789" onkeyup="saltar(event,'pallets2')" type="text" />
                                        </div>
                                        <div class="col-md-3" style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets2" name="pallets2" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas2')" />
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;" >
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                           <input id="pzas2" name="pzas2" maxlength="4" style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery3')" />
                                        </div>                                        
                                    </div>
                                    <!--(3)-->
                                    <div class="row no" id="componentes3" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery3" name="delivery3" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets3')" type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets3" name="pallets3" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas3')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                           <input id="pzas3" name="pzas3" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery4')"/>
                                        </div>                                        
                                    </div>
                                    <!--(4)-->
                                    <div class="row no" id="componentes4">
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery4" name="delivery4" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets4')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets4" name="pallets4" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas4')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas4" name="pzas4" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery5')" />
                                        </div>                                        
                                    </div>
                                    <!--(5)-->
                                    <div class="row no" id="componentes5" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery5" name="delivery5" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets5')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets5" name="pallets5" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas5')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas5" name="pzas5" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery6')" />
                                        </div>                                        
                                    </div>
                                    <!--(6)-->
                                    <div class="row no" id="componentes6" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery6" name="delivery6" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets6')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets6" name="pallets6" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas6')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas6" name="pzas6" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery7')"/>
                                        </div>                                        
                                    </div>
                                    <!--(7)-->
                                    <div class="row no" id="componentes7" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery7" name="delivery7" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets7')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets7" name="pallets7" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas7')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas7" name="pzas7" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery8')"/>
                                        </div>                                        
                                    </div>
                                    <!--(8)-->
                                    <div class="row no" id="componentes8" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery8" name="delivery8" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets8')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets8" name="pallets8" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas8')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas8" name="pzas8" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery9')"/>
                                        </div>                                        
                                    </div>
                                    <!--(9)-->
                                    <div class="row no" id="componentes9" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery9" name="delivery9" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets9')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets9" name="pallets9" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas9')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;" >
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas9" name="pzas9" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery10')"/>
                                        </div>                                        
                                    </div>
                                    <!--(10)-->
                                    <div class="row no" id="componentes10" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery10" name="delivery10" minlength="10" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets10')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets10" name="pallets10" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas10')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas10" name="pzas10" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery11')"/>
                                        </div>                                        
                                    </div>
                                    <!--(11)-->
                                    <div class="row no" id="componentes11" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery11" name="delivery11" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets11')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets11" name="pallets11" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas11')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas11" name="pzas11" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery12')"/>
                                        </div>                                        
                                    </div>
                                    <!--(12)-->
                                    <div class="row no" id="componentes12" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery12" name="delivery12" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets12')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets12" name="pallets12" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas12')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas12" name="pzas12" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery13')"/>
                                        </div>                                        
                                    </div>
                                    <!--(13)-->
                                    <div class="row no" id="componentes13" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery13" name="delivery13" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets13')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets13" name="pallets13" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas13')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas13" name="pzas13" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery14')"/>
                                        </div>                                        
                                    </div>
                                    <!--(14)-->
                                    <div class="row no" id="componentes14" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery14" name="delivery14" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets14')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets14" name="pallets14" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas14')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas14" name="pzas14" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery15')"/>
                                        </div>                                        
                                    </div>
                                    <!--(15)-->
                                    <div class="row no" id="componentes15" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery15" name="delivery15" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets15')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets15" name="pallets15" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas15')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas15" name="pzas15" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery16')"/>
                                        </div>                                        
                                    </div>
                                    <!--(16)-->
                                    <div class="row no" id="componentes16" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery16" name="delivery16" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets16')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets16" name="pallets16" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas16')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas16" name="pzas16" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery17')"/>
                                        </div>                                        
                                    </div>
                                    <!--(17)-->
                                    <div class="row no" id="componentes17" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery17" name="delivery17" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets17')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets17" name="pallets17" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas17')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas17" name="pzas17" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery18')"/>
                                        </div>                                        
                                    </div>
                                    <!--(18)-->
                                    <div class="row no" id="componentes18" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery18" name="delivery18" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets18')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets18" name="pallets18" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas18')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas18" name="pzas18" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery19')"/>
                                        </div>                                        
                                    </div>
                                    <!--(19)-->
                                    <div class="row no" id="componentes19" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery19" name="delivery19" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets19')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets19" name="pallets19" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas19')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas19" name="pzas19" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'delivery20')"/>
                                        </div>                                        
                                    </div>
                                    <!--(20)-->
                                    <div class="row no" id="componentes20" >
                                        <div class="col-md-4"> 
                                            <span style="margin-left: -16px;"><b> Delivery: </b></span>  
                                            <input id="delivery20" name="delivery20" maxlength="10" style="margin-left: -2px; width: 65%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" onkeyup="saltar(event,'pallets20')"  type="text"  />
                                        </div>
                                        <div class="col-md-3"style="margin-left: 7px;" >
                                            <span style="margin-left: -15px;" ><b> Pallets: </b></span>
                                            <input id="pallets20" name="pallets20" maxlength="3" style=" width: 58%;" onkeypress="return permite(event,'num')"; placeholder="123" onkeyup="saltar(event,'pzas20')"/>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px;">
                                           <span style="margin-left: -16px;"><b > Piezas: </b></span>
                                            <input id="pzas20" name="pzas20" maxlength="4"  style="width: 59%;" onkeypress="return permite(event,'num')"; placeholder="1234" onkeyup="saltar(event,'placa')" />
                                        </div>                                        
                                    </div>
                                </div> 
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos de Transporte</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Placa: </b></span>
                                            <input id="placa" name="placa" style="width: 71%;" maxlength="12" placeholder="" type="text"  />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b>Ruta: </b></span>
                                            <input id="ruta" name="ruta" style="width: 78%;" maxlength="50" placeholder="" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <select id="dock" name="dock" style="width: 100%;" required >
                                                <!--<option selected="false"  >Dock</option>-->
                                                <option value="1" >Dock 1</option>
                                                <option value="2" >Dock 2</option>
<!--                                                <option value="3" >Dock 3</option>
                                                <option value="4" >Dock 4</option>-->
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="row"  style="margin-top: 5px">
                                        <div class="col-md-7">
                                            <span><b> Chofer: </b></span> 
                                            <input id="chofer" name="chofer" style="width: 75%;" maxlength="50" placeholder="" type="text" onkeypress="return permite(event,'car')"; onkeyup="saltar(event,'modeTransport')"  />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b> Mode Transport: </b></span> 
                                            <select id="modeTransport" name="modeTransport" style="float: right; width: 42%; " required>
                                                <option value="0" selected="false" disabled="true" >Seleccion </option>
                                                <option value="1" >Aereo</option>
                                                <option value="2" >Maritimo</option>
                                                <option value="3" >Terrestre</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos Complementarios</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <span><b>Nota: </b></span>
                                            <textarea id="nota" name="nota" style= "height: 30px; width: 92%;" maxlength="248" placeholder="" type="text" > </textarea>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div >
                        <div class="modal-footer" style="margin-top: -25px">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
                        </div>
                    </div>
                </div><!-- /.modal -->         
            </div>          
        </form>
</body>