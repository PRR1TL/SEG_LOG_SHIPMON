<head>    
    <?php 
        //CONFIGURACION DE FECHAS DE LOS PICKERS
        if ($_SESSION['tipo'] == 1){
            $fInicio = date('d/m/Y', strtotime('+0 day')) ; 
            $fLimite = date('d/m/Y', strtotime('+30 day'));
            $fLimiteFin = date('d/m/Y', strtotime('+3 month')); 
        }else{
            $fInicio = date('d/m/Y', strtotime('+1 day')); 
            $fLimite = date('d/m/Y', strtotime('+7 day')); 
            $fLimiteFin = date('d/m/Y', strtotime('+1 month')); 
        }
        
        //AQUI SE OBTIENEN TODAS LAS HORAS POR RANGOS DE 15 MIN
        for ($i = 0; $i < 24; $i++){    
            if ($i < 10){
                $i = '0'.$i;
            }
            if ($i == 0){
                $x = 0;
            } else {
                $x = $i * 4;
            } 
            for ($j = 0; $j < 60; $j+=15 ){
                if ($j < 10){
                    $horas[$x] = $i.':0'.$j;
                }else{
                    $horas[$x] = $i.':'.$j;
                }
                $x+=1;
            }
        }        
        
        //CONEXION A BD        
        $server = "SGLERSQL01\sqlexpress, 1433";  
        $database = "DB_LER_SHIPMON_SQL";  
        $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");
        
    ?>
    
    <style>
        .no {
            display:none;
        }
        .si {
            display:block;
        }
    </style>
    
    <script type="text/javascript">             
        //DATE PICKER
        $( function() {
            $( "#fechaU" ).datepicker({
                minDate: <?php echo "'$fInicio'" ?> , 
                maxDate: <?php echo "'$fLimite'" ?>,
                dateFormat: 'dd/mm/yy'
            });
        } );
        
        $( function() {
            $( "#fechaFinU" ).datepicker({
                minDate: <?php echo "'$fInicio'" ?> , 
                maxDate: <?php echo "'$fLimiteFin'" ?>,
                dateFormat: 'dd/mm/yy'
            });
        } );
        
        //FUNCION PARA CUANDO SE HACE EL CAMBIO DE SOLD TO PARTY
        //AQUI SE LLENA EL INPUT DEL NOMBRE DEL CLIENTE (SOLD TO PARTY)
        function cambioOpciones(){ 
            var id = document.getElementById('soldToU').value;

            var dataString = 'action='+ id;
            //console.log('entro2');

            $.ajax({
                url: './db/getClienteNameU.php',
                data: dataString,
                cache: false,
                success: function(r){
                    $("#showIdU").html(r);
//                        alert('Termino');
                } 
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
              });                        
        }
        
        function cambioEstado(){
            var estado = document.getElementById('estadoU').value;
            console.log(estado);
            if (estado == 5){
                document.getElementById("componentesFacura").style.display = "block";
                //componentesFacura.className='si';
            } else {
                document.getElementById("componentesFacura").style.display = "none";
                //componentesFacura.className='no';
            }
        }
        
        function permite(elEvento, permitidos) {
            // Variables que definen los caracteres permitidos
            var numeros = "0123456789";
            var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            var numeros_caracteres = numeros + caracteres;
            var teclas_especiales = [8, 37, 39, 46];
            // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
            // Seleccionar los caracteres a partir del parámetro de la función
            switch(permitidos) {
              case 'num':
                permitidos = numeros;
                break;
              case 'car':
                permitidos = caracteres;
                break;
              case 'num_car':
                permitidos = numeros_caracteres;
                break;
            }
            // Obtener la tecla pulsada 
            var evento = elEvento || window.event;
            var codigoCaracter = evento.charCode || evento.keyCode;
            var caracter = String.fromCharCode(codigoCaracter);

            // Comprobar si la tecla pulsada es alguna de las teclas especiales
            // (teclas de borrado y flechas horizontales)
            var tecla_especial = false;
            for(var i in teclas_especiales) {
                if(codigoCaracter == teclas_especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }
            // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
            // o si es una tecla especial
            return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }
        
        function addBotones(){
            document.getElementById("componentes2U").style.display = "block";
            //document.getElementById("addModulo").style.display = "none";
        }
        
        function addBoton3(){
            document.getElementById("componentes3U").style.display = "block";
            //document.getElementById("addModulo2").style.display = "none";
        }
        
        function addBoton4(){
            document.getElementById("componentes4U").style.display = "block";
            //document.getElementById("addModulo3").style.display = "none";
        }
        
        function addBoton5(){
            document.getElementById("componentes5U").style.display = "block";
            //document.getElementById("addModulo4").style.display = "none";
        }
        
        function addBoton6(){
            document.getElementById("componentes6U").style.display = "block";
            //document.getElementById("addModulo5").style.display = "none";
        }
        
        function addBoton7(){
            document.getElementById("componentes7U").style.display = "block";
            //document.getElementById("addModulo6").style.display = "none";
        }
        
        function addBoton8(){
            document.getElementById("componentes8U").style.display = "block";
            //document.getElementById("addModulo7").style.display = "none";
        }
        
        function addBoton9(){
            document.getElementById("componentes9U").style.display = "block";
            //document.getElementById("addModulo8").style.display = "none";
        }
        
        function addBoton10(){
            document.getElementById("componentes10U").style.display = "block";
            //document.getElementById("addModulo9").style.display = "none";
        }
        
        function addBoton11(){
            document.getElementById("componentes11U").style.display = "block";
            //document.getElementById("addModulo10").style.display = "none";
        }
        
        function addBoton12(){
            document.getElementById("componentes12U").style.display = "block";
            //document.getElementById("addModulo11").style.display = "none";
        }
        
        function addBoton13(){
            document.getElementById("componentes13U").style.display = "block";
            //document.getElementById("addModulo12").style.display = "none";
        }
        
        function addBoton14(){
            document.getElementById("componentes14U").style.display = "block";
            //document.getElementById("addModulo13").style.display = "none";
        }
        
        function addBoton15(){
            document.getElementById("componentes15U").style.display = "block";
            //document.getElementById("addModulo14").style.display = "none";
        }
        
        function addBoton16(){
            document.getElementById("componentes16U").style.display = "block";
            //document.getElementById("addModulo15").style.display = "none";
        }
        
        function addBoton17(){
            console.log("addBoton3");
            document.getElementById("componentes17U").style.display = "block";
            //document.getElementById("addModulo16").style.display = "none";
        }
        
        function addBoton18(){
            document.getElementById("componentes18U").style.display = "block";
            //document.getElementById("addModulo17").style.display = "none";
        }
        
        function addBoton19(){
            document.getElementById("componentes19U").style.display = "block";
            //document.getElementById("addModulo18").style.display = "none";
        }
        
        function addBoton20(){
            document.getElementById("componentes20U").style.display = "block";
            //document.getElementById("addModulo19").style.display = "none";
        }
        
        //MODULO DE OCULTAR BOTONES
        function deleteBoton2(){
            document.getElementById("delivery2U").value = "";
            document.getElementById("pallets2U").value = "";
            document.getElementById("pzas2U").value = "";
            document.getElementById("componentes2U").style.display = "none";
            //document.getElementById("addModulo").style.display = "none";
        }
        
        function deleteBoton3(){
            document.getElementById("delivery3U").value = "";
            document.getElementById("pallets3U").value = "";
            document.getElementById("pzas3U").value = "";
            document.getElementById("componentes3U").style.display = "none";
           //document.getElementById("addModulo2").style.display = "block";
        }
        
        function deleteBoton4(){
            document.getElementById("delivery4U").value = "";
            document.getElementById("pallets4U").value = "";
            document.getElementById("pzas4U").value = "";
            document.getElementById("componentes4U").style.display = "none";
            //document.getElementById("addModulo3").style.display = "block";
        }
        
        function deleteBoton5(){
            document.getElementById("delivery5U").value = "";
            document.getElementById("pallets5U").value = "";
            document.getElementById("pzas5U").value = "";
            document.getElementById("componentes5U").style.display = "none";
            //document.getElementById("addModulo4").style.display = "block";
        }
        
        function deleteBoton6(){
            document.getElementById("delivery6U").value = "";
            document.getElementById("pallets6U").value = "";
            document.getElementById("pzas6U").value = "";
            document.getElementById("componentes6U").style.display = "none";
            //document.getElementById("addModulo5").style.display = "block";
        }
        
        function deleteBoton7(){
            document.getElementById("delivery7U").value = "";
            document.getElementById("pallets7U").value = "";
            document.getElementById("pzas7U").value = "";
            document.getElementById("componentes7U").style.display = "none";
            //document.getElementById("addModulo6").style.display = "block";
        }
        
        function deleteBoton8(){
            document.getElementById("delivery8U").value = "";
            document.getElementById("pallets8U").value = "";
            document.getElementById("pzas8U").value = "";
            document.getElementById("componentes8U").style.display = "none";
            //document.getElementById("addModulo7").style.display = "block";
        }
        
        function deleteBoton9(){
            document.getElementById("delivery9U").value = "";
            document.getElementById("pallets9U").value = "";
            document.getElementById("pzas9U").value = "";
            document.getElementById("componentes9U").style.display = "none";
            //document.getElementById("addModulo8").style.display = "block";
        }
        
        function deleteBoton10(){
            document.getElementById("delivery10U").value = "";
            document.getElementById("pallets10U").value = "";
            document.getElementById("pzas10U").value = "";
            document.getElementById("componentes10U").style.display = "none";
            //document.getElementById("addModulo9").style.display = "block";
        }
        
        function deleteBoton11(){
            document.getElementById("delivery11U").value = "";
            document.getElementById("pallets11U").value = "";
            document.getElementById("pzas11U").value = "";
            document.getElementById("componentes11U").style.display = "none";
            //document.getElementById("addModulo10").style.display = "block";
        }
        
        function deleteBoton12(){
            document.getElementById("delivery12U").value = "";
            document.getElementById("pallets12U").value = "";
            document.getElementById("pzas12U").value = "";
            document.getElementById("componentes12U").style.display = "none";
           //document.getElementById("addModulo11").style.display = "block";
        }
        
        function deleteBoton13(){
            document.getElementById("delivery13U").value = "";
            document.getElementById("pallets13U").value = "";
            document.getElementById("pzas13U").value = "";
            document.getElementById("componentes13U").style.display = "none";
            //document.getElementById("addModulo12").style.display = "block";
        }
        
        function deleteBoton14(){
            document.getElementById("delivery14U").value = "";
            document.getElementById("pallets14U").value = "";
            document.getElementById("pzas14U").value = "";
            document.getElementById("componentes14U").style.display = "none";
            //document.getElementById("addModulo13").style.display = "block";
        }
        
        function deleteBoton15(){
            document.getElementById("delivery15U").value = "";
            document.getElementById("pallets15U").value = "";
            document.getElementById("pzas15U").value = "";
            document.getElementById("componentes15U").style.display = "none";
            //document.getElementById("addModulo14").style.display = "block";
        }
        
        function deleteBoton16(){
            document.getElementById("delivery16U").value = "";
            document.getElementById("pallets16U").value = "";
            document.getElementById("pzas16U").value = "";
            document.getElementById("componentes16U").style.display = "none";
            //document.getElementById("addModulo15").style.display = "block";
        }
        
        function deleteBoton17(){
            document.getElementById("delivery17U").value = "";
            document.getElementById("pallets17U").value = "";
            document.getElementById("pzas17U").value = "";
            document.getElementById("componentes17U").style.display = "none";
            //document.getElementById("addModulo16").style.display = "block";
        }
        
        function deleteBoton18(){
            document.getElementById("delivery18U").value = "";
            document.getElementById("pallets18U").value = "";
            document.getElementById("pzas18U").value = "";
            document.getElementById("componentes18U").style.display = "none";
            //document.getElementById("addModulo17").style.display = "block";
        }
        
        function deleteBoton19(){
            document.getElementById("delivery19U").value = "";
            document.getElementById("pallets19U").value = "";
            document.getElementById("pzas19U").value = "";
            document.getElementById("componentes19U").style.display = "none";
            //document.getElementById("addModulo18").style.display = "block";
        }
        
        function deleteBoton20(){
            document.getElementById("delivery20U").value = "";
            document.getElementById("pallets20U").value = "";
            document.getElementById("pzas20U").value = "";
            document.getElementById("componentes20U").style.display = "none"; 
            //document.getElementById("addModulo19").style.display = "block";
        }
        
//        VALIDACION PARA LOS RADIO BUTOM DE FRECUENCIA (M: MES / S:SEMANA)
        var era;
        var previo=null;

        function uncheckRadio(rbutton){
            //console.log(rbutton);
            if(previo && previo != rbutton){
                previo.era = false;
                //console.log("1");
            }
            if(rbutton.checked == true && rbutton.era == true){
                rbutton.checked = false;
                //console.log("2");
            }
            
            if (rbutton.checked == true ){
                //AQUI HACEMOS VISIBLE EL DIV DE CANTIDAD Y DIAS
                document.getElementById("datPeriodoU").style.display = "block";
                //datPeriodoU.className='si';
            }else {
                document.getElementById("datPeriodoU").style.display = "none";
                //datPeriodoU.className='no';
            }
            console.log("era: "+rbutton.era+ " cheked : "+ rbutton.checked);
            
            rbutton.era = rbutton.checked;
            previo=rbutton;
        }
        
    </script>
</head>

<body>
    <?php 
        $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo']:0 ;
        $dia = 26;
        $hInicio = 15;
        $hFin = 18;
    ?>
    
        <form id="modificarEvento" method="post">
            <div id="updateEvent" class="modal fade" tabindex="-1" role="dialog">            
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background:#a9cce3;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 align=center class="modal-title">MODIFICACIÓN</h4>
                        </div>
                        <div class="modal-body" > 
                            <div id="datos_ajax"></div>
                            <div class="panel panel-default">
                                <div class="panel-heading" >
                                    <h3 class="panel-title">Datos de Evento</h3>
                                </div>
                                <div class="panel-body">                                     
                                    <div class="row">
                                        <div class="col-md-3" style="margin-left: -5px; " >
                                            <input id="idEventU" name="idEventU" class="no" >
                                            <input id="idEventRelacion" name="idEventRelacion" class="no" >
                                            <span name="dia" align="center" ><b> Día: </b></span>
                                            <input type="text" name="fechaU" id="fechaU" placeholder="dd/mm/yyyy" class="form-control input" style="margin-top: -28px; margin-left: 30px; width: 90%" disabled="disabled">
                                            <!-- <input id="fecha" name="fecha" type="text" style="width: 70%;" value="<?php echo "$fInicio" ?>"  > -->
                                        </div>
                                        <div class="col-md-5" style="margin-left: 5px; ">
                                            <span name="fecha" align="center" > <b> Hora: </b></span>                                        
                                            <select class="cmbFecha" name="startU" id="startU" placeholder="Inicio" style="width: 35%" disabled="disabled">
                                                <?php 
                                                    for ($x = 0; $x < 96; $x++){ 
                                                        echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                       
                                                    } 
                                                ?>     
                                            </select>                               

                                            <span><b> a </b></span>
                                            <select class="cmbFecha" name="endU" id="endU" placeholder="Inicio" style="width: 35%" disabled="disabled">
                                                <?php 
                                                    for ($x = 0; $x < 96; $x++){ 
                                                        echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                       
                                                    } 
                                                ?> 
                                            </select>
                                        </div>
                                        <?php if($tipo == 1){ ?>                     
                                            <div class="col-md-2">
                                                <select id="userAsigU" name="userAsigU" style=" float: right; width: 125%" >
                                                    <option value='0' selected disabled="disabled"> Usuario </option>
                                                    <?PHP 
                                                        if ( $conn ){
                                                            $stmt = $conn->query( "SELECT usuario FROM usuarios" );  
                                                            $result = sqlsrv_query($conn,$stmt);  

                                                            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                                extract($row);
                                                                echo "<option value='".$usuario."'>" . $usuario. "</option>";
                                                            }
                                                            $result = sqlsrv_close($conn);
                                                        } else {
                                                            echo "<option value='0' selected> ERROR: 218 mIEvento </option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>                                            
                                        <?php } ?>
                                        <div class="col-md-2" id="divFijos"> 
                                            <label class="form-check-label">
                                                Fija
                                                <input class="form-check-input" type="radio" name="periodoTiempoU" id="periodoTiempoU" onclick="uncheckRadio(this)" value="1" > 
                                            </label>
                                            
                                        </div>
                                    </div>                                    
                                    <div class="row no" id="datPeriodoU" name='datPeriodoU' style="margin-top: 5px" >
                                        <div class="col-md-3" style="margin-top: 2px; margin-left: -5px" >
                                            <span name="diaU" align="center" ><b> Fin: </b></span>
                                            <input type="text" name="fechaFinU" id="fechaFinU" placeholder="dd/mm/yyyy" class="form-control input" value="" style="margin-top: -20px; margin-left: 30px; width: 91%" disabled="disabled" >
                                        </div>
                                        <div class="col-md-8" style="margin-top: 5px">
                                            
                                            <label class="form-check-label" style="margin-left: 5px">
                                                <input class="form-check-input" type="checkbox" name="d7U" id="d7U" value="7" disabled="disabled" > D
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">                                                
                                                <input class="form-check-input" type="checkbox" name="d1U" id="d1U" value="1" disabled="disabled"> L
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d2U" id="d2U" value="2" disabled="disabled"> M
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d3U" id="d3U" value="3" disabled="disabled"> Mi
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d4U" id="d4U" value="4" disabled="disabled"> J
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d5U" id="d5U" value="5" disabled="disabled"> V
                                            </label>
                                            
                                            <label class="form-check-label" style="margin-left: 12px">
                                                <input class="form-check-input" type="checkbox" name="d6U" id="d6U" value="6" disabled="disabled"> S
                                            </label>                                            
                                            
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="panel panel-default" style="margin-top: -10px">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Envio</h3>
                                </div>
                                <div class="panel-body">      
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Ship to : </b></span>
                                            <!--onchange='cambioOpciones();'-->
                                            <select style=" float: right; width: 64%; display: block;" id='soldToU' name="soldToU" onchange='cambioOpciones();' disabled="disabled" >
                                                <option value="0" selected="false" disabled="true"selected > Seleccion </option>
                                                <?php
                                                    $server = "SGLERSQL01\sqlexpress, 1433";  
                                                    $database = "DB_LER_SHIPMON_SQL";  
                                                    $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");

                                                    if ( $conn ){
                                                        $stmt = $conn->query("SELECT * FROM clientes" );  
                                                        $result = sqlsrv_query($conn,$stmt);  

                                                        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                            extract($row);
                                                            echo "<option value='".$soldToParty."'>" . $soldToParty. "</option>";
                                                        }
                                                        $result = sqlsrv_close($conn);
                                                    } else {
                                                        echo "<option value='0' selected> ERROR: 173 mIEvento </option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-8" id="showIdU" >
                                            <input style="float: right; width: 100%" name="nameClienteU" id="nameClienteU" readonly />
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px" >
                                        <div class="col-md-4" >
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery1U" name="delivery1U"  minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets1U" name="pallets1U" maxlength="3" style="float: right; width: 54%;" onkeypress="return permite(event,'num')"; placeholder="123"  />
                                        </div>
                                        
                                        <div class="col-md-3" >
                                            <span ><b > Piezas: </b></span>
                                            <input id="pzas1U" name="pzas1U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234"  />
                                        </div>  
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo" onClick="addBotones()" value="+"  />
                                        </div>
                                    </div> 
                                    <!--AQUI INICIA EL APARTADO DE DIVS OCULTOS-->
                                    <!--(2)-->
                                    <div class="row no" id="componentes2U" >
                                        <div class="col-md-4" >
                                            <span ><b> Delivery: </b></span>  
                                            <input id="delivery2U" name="delivery2U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text" />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets2U" name="pallets2U" maxlength="3" style="float: right; width: 54%;" onkeypress="return permite(event,'num')"; placeholder="123" />
                                        </div>
                                        <div class="col-md-3" >
                                           <span ><b > Piezas: </b></span>
                                           <input id="pzas2U" name="pzas2U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>   
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo2" onClick="addBoton3()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo2" onClick="deleteBoton2()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(3)-->
                                    <div class="row no" id="componentes3U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery3U" name="delivery3U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets3U" name="pallets3U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                           <input id="pzas3U" name="pzas3U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo3" onClick="addBoton4()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo3" onClick="deleteBoton3()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(4)-->
                                    <div class="row no" id="componentes4U">
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery4U" name="delivery4U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets4U" name="pallets4U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas4U" name="pzas4U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo4" onClick="addBoton5()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo4" onClick="deleteBoton4()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(5)-->
                                    <div class="row no" id="componentes5U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery5U" name="delivery5U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets5U" name="pallets5U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas5U" name="pzas5U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo5" onClick="addBoton6()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo5" onClick="deleteBoton5()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(6)-->
                                    <div class="row no" id="componentes6U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery6U" name="delivery6U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets6U" name="pallets6U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas6U" name="pzas6U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo6" onClick="addBoton7()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo6" onClick="deleteBoton6()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(7)-->
                                    <div class="row no" id="componentes7U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery7U" name="delivery7U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets7U" name="pallets7U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas7U" name="pzas7U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo7" onClick="addBoton8()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo7" onClick="deleteBoton7()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(8)-->
                                    <div class="row no" id="componentes8U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery8U" name="delivery8U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets8U" name="pallets8U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas8U" name="pzas8U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo8" onClick="addBoton9()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo8" onClick="deleteBoton8()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(9)-->
                                    <div class="row no" id="componentes9U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery9U" name="delivery9U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets9U" name="pallets9U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3" >
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas9U" name="pzas9U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo9" onClick="addBoton10()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo9" onClick="deleteBoton9()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(10)-->
                                    <div class="row no" id="componentes10U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery10U" name="delivery10U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets10U" name="pallets10U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas10U" name="pzas10U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo10" onClick="addBoton11()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo10" onClick="deleteBoton10()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(11)-->
                                    <div class="row no" id="componentes11U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery11U" name="delivery11U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets11U" name="pallets11U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas11U" name="pzas11U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo11" onClick="addBoton12()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo11" onClick="deleteBoton11()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(12)-->
                                    <div class="row no" id="componentes12U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery12U" name="delivery12U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets12U" name="pallets12U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas12U" name="pzas12U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo12" onClick="addBoton13()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo12" onClick="deleteBoton12()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(13)-->
                                    <div class="row no" id="componentes13U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery13U" name="delivery13U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets13U" name="pallets13U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas13U" name="pzas13U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo13" onClick="addBoton14()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo13" onClick="deleteBoton13()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(14)-->
                                    <div class="row no" id="componentes14U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery14U" name="delivery14U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets14U" name="pallets14U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas14U" name="pzas14U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo14" onClick="addBoton15()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo14" onClick="deleteBoton14()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(15)-->
                                    <div class="row no" id="componentes15U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery15U" name="delivery15U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets15U" name="pallets15U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas15U" name="pzas15U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo15" onClick="addBoton16()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo15" onClick="deleteBoton15()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(16)-->
                                    <div class="row no" id="componentes16U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery16U" name="delivery16U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets16U" name="pallets16U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas16U" name="pzas16U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo16" onClick="addBoton17()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo16" onClick="deleteBoton16()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(17)-->
                                    <div class="row no" id="componentes17U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery17U" name="delivery17U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets17U" name="pallets17U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas17U" name="pzas17U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo17" onClick="addBoton18()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo17" onClick="deleteBoton17()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(18)-->
                                    <div class="row no" id="componentes18U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery18U" name="delivery18U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets18U" name="pallets18U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas18U" name="pzas18U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo18" onClick="addBoton19()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo18" onClick="deleteBoton18()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(19)-->
                                    <div class="row no" id="componentes19U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery19U" name="delivery19U" minlength="10" maxlength="10" style="float: right; width: 62%;" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets19U" name="pallets19U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas19U" name="pzas19U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-warning" id="addModulo19" onClick="addBoton20()" value="+"  />
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo19" onClick="deleteBoton19()" value=" - "  />
                                        </div>
                                    </div>
                                    <!--(20)-->
                                    <div class="row no" id="componentes20U" >
                                        <div class="col-md-4"> 
                                            <span ><b> Delivery: </b></span>   
                                            <input id="delivery20U" name="delivery20U" minlength="10" maxlength="10" style="float: right; width: 62%;" onkeypress="return permite(event,'num')"; placeholder="0123456789" type="text"  />
                                        </div>
                                        <div class="col-md-3" >
                                            <span ><b> Pallets: </b></span>
                                            <input id="pallets20U" name="pallets20U" maxlength="3" style="float: right; width: 54%;"onkeypress="return permite(event,'num')"; placeholder="123"/>
                                        </div>
                                        <div class="col-md-3">
                                           <span ><b > Piezas: </b></span>
                                            <input id="pzas20U" name="pzas20U" maxlength="4" style="float: right; width: 55%;" onkeypress="return permite(event,'num')"; placeholder="1234" />
                                        </div>                                       
                                        <div class="col-md-2" >
                                            <input type="button" class="btn btn-xs btn-danger" id="deleteModulo20" onClick="deleteBoton20()" value=" - "  />
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos de Transporte</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Placa: </b></span>
                                            <input id="placaU" name="placaU" style="width: 71%;" maxlength="12" placeholder="" type="text" />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b>Ruta: </b></span>
                                            <input id="rutaU" name="rutaU" style="width: 78%;" maxlength="50" placeholder="" type="text" />
                                        </div>
                                        <div class="col-md-3" >
                                            <select id="dockU" name="dockU" style="width: 100%;"  disabled="disabled">
                                                <option value="0" selected="false" disabled="true" >Dock</option>
                                                <option value="1" >Dock 1</option>
                                                <option value="2" >Dock 2</option>
                                                <option value="3" >Dock 3</option>
                                                <option value="4" >Dock 4</option>
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-7">
                                            <span><b> Chofer: </b></span> 
                                            <input id="choferU" name="choferU" style="width: 75%;" maxlength="50" placeholder="" type="text" onkeypress="return permite(event,'car')" />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b> Mode Transport: </b></span> 
                                            <select id="modeTransportU" name="modeTransportU" style="float: right; width: 42%; " >
                                                <option value="0" selected="false" disabled="true" >Seleccion </option>
                                                <option value="1" >Aereo</option>
                                                <option value="2" >Maritimo</option>
                                                <option value="3" >Terrestre</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos Complementarios</h4>
                                </div>
                                <div class="panel-body">
                                    <?PHP if ($tipo == 1 ) { ?>
                                        <div class="row">
                                            <div class="col-md-6 " >
                                                <span><b>Estado: </b></span>
                                                <!--style= "height: 30px; width: 92%;" maxlength="248" placeholder="" type="text"-->
                                                <select id="estadoU" name="estadoU" onchange='cambioEstado()'> 
                                                    <option value="" selected="false" disabled="true" selected > Seleccion </option>
                                                    <option value="1"> Shipment Creado </option>
                                                    <option value="2"> Loading Start </option>
                                                    <option value="3"> Shipment Start </option>
                                                    <option value="4"> Shipment Invoiced </option>
                                                    <option value="5"> Shipment Complete </option>
                                                    <option value="6"> Delay </option>                                              
                                                    <option value="0"> Delete </option>                                              
                                                </select>
                                            </div>
                                            <div class="col-md-6 no" id="componentesFacura" >
                                                <span><b>Núm Factura: </b></span>
                                                <input id="factura" name="factura" maxlength="12" style= " width: 60%;"/> 
                                            </div>
                                        </div> 
                                    <?PHP }?>
                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-12" >
                                            <span><b>Nota: </b></span>
                                            <textarea id="notaU" name="notaU" style= "height: 30px; width: 92%;" maxlength="248" placeholder="" type="text"> </textarea>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div >
                        <div class="modal-footer" style="margin-top: -25px">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
                        </div>
                    </div>
                </div><!-- /.modal -->         
            </div>          
        </form>
</body>