<form id="logiin" method="post">
    <div id="login" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 align=center class="modal-title">Bienvenido</h4>
                </div>
                <div class="modal-body">
                    <div id="datos_ajax"></div> 
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="username"  placeholder="Usuario"/>
                        </div>
                    </div>
                    <br>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password"  placeholder="Contraseña"/>
                        </div>
                    </div>
                    
                    <!--BOTONES-->
                    <br>
                    <div class="modal-footer"> 
                        <button type="submit" class="btn btn-primary">Entrar <i class="glyphicon glyphicon-log-in"></i></button>
                        <button id="btnCerrar" type="button" class="btn btn-default" data-dismiss="modal"> Cerrar </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>    
</form>