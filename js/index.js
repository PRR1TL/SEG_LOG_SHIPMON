$(document).ready(function() {
    //OBTENESMOS LA HORA EN TIEMPO REAL
    //var fHNow = '2018-06-21T09:25:00';
    var fHNow = moment(new Date());
    var today = moment(new Date()).format("HH");
    var vToday = new Date().getDay();

    //FUNCIONES PARA EL CALENDARIO 
    var calendar = $('#calendar').fullCalendar({   
            nowIndicator: true,
            now: fHNow,
            defaultDate: fHNow,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            axisFormat: 'H:mm',
            timeFormat: {
                agenda: 'H:mm'
            },
            //height: 'auto',
            height: 620,
            events: './db/load.php',
            defaultView: 'agendaWeek',
            slotDuration: '00:15',
            buttonText: ':15 slots',
            navLinks: true, // can click day/week names to navigate views
            selectable: false,
            eventRender: function(eventObj, $el) {
                $el.popover({
                    title: eventObj.usuario ,
                    content: "Ship To: \n"+eventObj.title,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body'
                });
            },
            //AQUI INICIA EL MODULO PARA EDICION DE EVENTOS
            eventClick: function(event) { 
                //TODOS LOS PARAMETROS SE SE OBTENGAN, SE DEBEN EVALUAR COMO CADENA
                var descEstado;
                //EVALUAMOS EL ESTADO PARA MANDAR LA LEYENDA DEL ESTADO
                switch (event.estado){
                    case '1':
                        descEstado = "Shipment Creado";
                        $('#divFac').hide();
                        break;
                    case '2':
                        descEstado = "Loading Start";
                        $('#divFac').hide();
                        break;
                    case '3':
                        descEstado = "Shipment Start";
                        $('#divFac').hide();
                        break;
                    case '4':
                        descEstado = "Shipment Invoiced";
                        $('#divFac').hide();
                        break;
                    case '5':
                        descEstado = "Shipment Complete";
                        $('#divFac').show();
                        $('#facturaCons').val(event.numFac);
                        break;
                    case '6':
                        descEstado = "Delay";
                        $('#divFac').hide();
                        break;                        
                }
                //EVALUAMOS EL TIPO DE REGISTRO, PARA MANDAR LAS CAJAS CORRESPONDIENTES
                switch (event.tipoRegistro){
                    case '1':
                    case '1.1':
                    case '1.2': 
                        $('#fechaFCons').hide();  
                        $('#diasFijos').hide();
                        $('#divEstado').show();
                        
                        break;
                    case '2':
                    case '2.1':
                    case '2.2':
                        $('#fechaFCons').show();  
                        $('#diasFijos').show();
                        break;                                       
                }
                
                //OBTENERMOS LOS VALORES DE LOS DELIVERYS
                var conI;
                var dl =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS DELIVERYS
                var pl =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS PALLETS
                var pz =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS PIEZAS
                
                dl[1] = event.dl1;
                pl[1] = event.pl1;
                pz[1] = event.pz1;

                dl[2] = event.dl2;
                pl[2] = event.pl2;
                pz[2] = event.pz2;

                dl[3] = event.dl3;
                pl[3] = event.pl3;
                pz[3] = event.pz3;

                dl[4] = event.dl4;
                pl[4] = event.pl4;
                pz[4] = event.pz4;

                dl[5] = event.dl5;
                pl[5] = event.pl5;
                pz[5] = event.pz5;

                dl[6] = event.dl6;
                pl[6] = event.pl6;
                pz[6] = event.pz6;

                dl[7] = event.dl7;
                pl[7] = event.pl7;
                pz[7] = event.pz7;

                dl[8] = event.dl8;
                pl[8] = event.pl8;
                pz[8] = event.pz8;

                dl[9] = event.dl9;
                pl[9] = event.pl9;
                pz[9] = event.pz9;
                
                dl[10] = event.dl10;
                pl[10] = event.pl10;
                pz[10] = event.pz10;

                dl[11] = event.dl11;
                pl[11] = event.pl11;
                pz[11] = event.pz11;

                dl[12] = event.dl12;
                pl[12] = event.pl12;
                pz[12] = event.pz12;

                dl[13] = event.dl13;
                pl[13] = event.pl13;
                pz[13] = event.pz13;

                dl[14] = event.dl14;
                pl[14] = event.pl14;
                pz[14] = event.pz14;

                dl[15] = event.dl15;
                pl[15] = event.pl15;
                pz[15] = event.pz15;

                dl[16] = event.dl16;
                pl[16] = event.pl16;
                pz[16] = event.pz16;

                dl[17] = event.dl17;
                pl[17] = event.pl17;
                pz[17] = event.pz17;

                dl[18] = event.dl18;
                pl[18] = event.pl18;
                pz[18] = event.pz18;

                dl[19] = event.dl19;
                pl[19] = event.pl19;
                pz[19] = event.pz19;     

                dl[20] = event.dl20;
                pl[20] = event.pl20;
                pz[20] = event.pz20;         
                
                for (x = 1; x < 21; x++ ){  
                    console.log(dl[x]);
                    if( dl[x] != "" && dl[x] != null ){ //EVALUAMOS SI TIENE CONTENIDO PARA MOSTRAR O NO LOS COMPONENTES
                        $('#componentes'+x+'Cons').show();
                        $('#delivery'+x+'Cons').val(dl[x]);
                        $('#pallets'+x+'Cons').val(pl[x]);
                        $('#pzas'+x+'Cons').val(pz[x]);
                    }else{
                        $('#componentes'+x+'Cons').hide(); 
                    }
                }
                
                //LANZA MODAL DE VISUALIZACION
                //alert(event.tipoRegistro);
                $('#idCons').val(event.id); 
                $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));  
                $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                $('#finCons').val(moment(event.end).format('HH:mm'));
                $('#userAsigCons').val(event.userAsig);
                $('#diaSelectCons').val(event.diasReg);
                $('#estadoCons').val(descEstado);

                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                $('#soldCons').val(event.shippTo);
                $('#nombreClienteCons').val(event.cliente);
                $('#deliveryCons').val(event.shippTo);  
                $('#cargaCons').val(event.pallets);
                $('#pzasCons').val(event.piezas); 
                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                $('#placaCons').val(event.placas);
                $('#rutaCons').val(event.ruta);
                $('#dockCons').val(event.puerta);    
                $('#choferCons').val(event.chofer);
                $('#modeTransportCons').val(event.modeTransport);

                //DATOS COMPLEMENTARIOS
                $('#notaCons').val(event.nota);

                //DATOS DE ESTADO
                $('#selectEvent').modal('show'); 
            }

        });
     
     
    //FUNCION PARA ACTUALIZAR LOS EVENTO CADA MINUTO
    var refreshId = setInterval(function() {
                    calendar.fullCalendar('refetchEvents');
                }, 50000);
    $.ajaxSetup({ cache: false });     
    
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/cLogin.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });    
});  