$(document).ready(function() {
    //OBTENESMOS LA HORA EN TIEMPO REAL
    var fHNow = moment(new Date());                     //OBTIENE LA FECHA COMPLETA CON HR
    var today = new Date().toISOString().slice(0,10);    //FECHA EN FORMATO (Y-m-d)
    var tF = moment(new Date()).add(1, 'days');         //FECHA ACTUAL + 1 DIA
    //var todayFormat = moment(new Date()).format("DD/MM/YYYY");    //FECHA EN FORMATO (dd/mm/dddd)
    var todayFormat = moment(tF).format("DD/MM/YYYY");    //FECHA EN FORMATO (dd/mm/dddd)
    var hToday = moment(new Date()).format("HH");        //HORA EN FORMATO 02
    var hmToday = moment(new Date()).format("HH:mm");      //HORA CON MINUTOS    
    var dToday = new Date().getDay();                   //DIA DE LA SEMANA
    
    var sessionUser = $('#usuario').val();
    
    //FUNCIONES PARA EL CALENDARIO     
    var calendar = $('#calendar').fullCalendar({     
            nowIndicator: true,
            now: fHNow,
            defaultDate: fHNow,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            axisFormat: 'H:mm',
            timeFormat: {
                agenda: 'H:mm'
            },
            contentHeight: 590,
            //height: 640,
            events: './db/load.php',
            textColor: 'black',
            defaultView: 'agendaWeek',
            slotDuration: '00:15',
            buttonText: ':15 slots',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            eventRender: function(eventObj, $el) {
                $el.popover({
                    title: eventObj.usuario+" ("+"Pt."+eventObj.puerta+") ",
                    content: "Ship To: \n"+eventObj.title,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body'
                });
            },
            //selectHelper: true,
            select: function(start, end, allDay) { 
                //VALIDA QUE SEA ENTRE SEMANA L-V
                if (dToday > 0 ){
                    if (dToday != 5 ){
                        //CUANDO SEA DISTINTO DE VIERNES
                        //VALIDACION PARA NO REGISTRAR DESPUES DE LAS 5 PM                        
                        if(hToday < 17 ) {
                            var dSelect = moment(start).format('DD/MM/YYYY');    
                            var hISelect = moment(start).format('HH:mm'); 
                            var hFSelect = moment(end).format('HH:mm'); 
                            //console.log(hISelect,", ",hFSelect);
                            //console.log("select: "+dSelect+" today: "+todayFormat);
                            if (dSelect >= todayFormat){
                                $('#fecha').val(dSelect);                                
                            }else {
                                $('#fecha').val(todayFormat);
                            }
                            $('#start').val(hISelect);
                            $('#end').val(hFSelect);
                            $('#newEvent').modal('show');
                        } else {
                            //LANZA EL MODAL DE LAS HORAS
                            $('#warningHora').modal('show');
                        }
                    }else {
                        //SI ES VIERNES ENTONCES DEJA HACER REGISTROS HASTA LAS 7 PM
                        if (dToday == 5 ){
                            if(hToday < 19 ) {
                                $('#newEvent').modal('show');
                            }else {
                                //SI ES VIERNES Y PASAN DE LAS 7 PM LANZA MODAL DE HORAS
                                $('#warningHora').modal('show');
                            }
                        }else { //SI ES SABADO ENTONCES DEJA ENTRAR HASTA LAS 12 DEL DIA
                            if(hToday < 12 ) {
                                $('#newEvent').modal('show');
                            }else {
                                //SI ES VIERNES Y PASAN DE LAS 7 PM LANZA MODAL DE HORAS
                                $('#warningHora').modal('show');
                            }
                        }
                    }
                } else{
                    //SI SE INTENTA HACR REGISTRO ENTRE FIN DE SEMANA LANZA MODAL DE FIN DE SEMANA
                    $('#warningFinSemana').modal('show');
                }
            },
            //AQUI INICIA EL MODULO PARA EDICION DE EVENTOS
            eventClick: function(event) { 
                //OBTENEMOS EL VALOR DE INICIO DEL EVENTO 
                var hIEvent = moment(event.start).format('HH');                
                var hAntes = hIEvent - hToday;
                
                //console.log("hAntes: "+hAntes);
                //TODOS LOS PARAMETROS SE SE OBTENGAN, SE DEBEN EVALUAR COMO CADENA
                var descEstado;
                //EVALUAMOS EL ESTADO PARA MANDAR LA LEYENDA DEL ESTADO
                //alert("estado: "+event.estado+" fac: "+event.numFac);
                
                //EVALUAMOS EL TIPO DE REGISTRO, PARA MANDAR LAS CAJAS CORRESPONDIENTES
                var cadDias;
                switch (event.tipoRegistro){
                    case '1':
                    case '1.1':
                    case '1.2': 
                        $('#fechaFCons').hide();  
                        $('#diasFijos').hide();
                        $('#divEstado').show();
                        $('#divFijos').hide();
                        $('#periodoTiempoU').prop('checked', false);   
                        switch (event.estado){
                            case '1':
                                descEstado = "Shipment Creado";
                                $('#divFac').hide();
                                break;
                            case '2':
                                descEstado = "Loading Start";
                                $('#divFac').hide();
                                break;
                            case '3':
                                descEstado = "Shipment Start";
                                $('#divFac').hide();
                                break;
                            case '4':
                                descEstado = "Shipment Invoiced";
                                $('#divFac').hide();
                                break;
                            case '5':
                                descEstado = "Shipment Complete";
                                $('#divFac').show();
                                $('#facturaCons').val(event.numFac);
                                break;
                            case '6':
                                descEstado = "Delay";
                                $('#divFac').hide();
                                break;                        
                        }
                        break;
                    case '2':                        
                    case '2.1':
                    case '2.2':
                        $('#fechaFCons').show();  
                        $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY')); 
                        $('#divFijos').show();
                        $('#datPeriodoU').show();
                        $('#fechaFinU').val(moment(event.finReg).format('DD/MM/YYYY')); 
                        $('#periodoTiempoU').prop('checked', true);
                        
                        switch (event.estado){
                            case '1':
                                descEstado = "Shipment Creado";
                                $('#divFac').hide();
                                break;
                            case '2':
                                descEstado = "Loading Start";
                                $('#divFac').hide();
                                break;
                            case '3':
                                descEstado = "Shipment Start";
                                $('#divFac').hide();
                                break;
                            case '4':
                                descEstado = "Shipment Invoiced";
                                $('#divFac').hide();
                                break;
                            case '5':
                                descEstado = "Shipment Complete";
                                $('#divFac').show();
                                $('#facturaCons').val(event.numFac);
                                break;
                            case '6':
                                descEstado = "Delay";
                                $('#divFac').hide();
                                break;                        
                        }
                        
                        //CORTAR LA CADENA DE LOS DIAS SELECCIONADOS PARA VENTANAS FIJAS
                        
                        cadDias = event.diasReg;
                        //console.log(cadDias);
                        var arrayDeCadena = cadDias.split(",");
                        for (var i = 0; i < arrayDeCadena.length; i++) {
                            //console.log(arrayDeCadena[i]);
                            switch (arrayDeCadena[i]){
                                case 'L':
                                    $('#d1U').prop('checked', true);
                                    break;
                                case 'M':
                                    $('#d2U').prop('checked', true);
                                    break;
                                case 'Mi':
                                    $('#d3U').prop('checked', true);
                                    break;
                                case 'J':
                                    $('#d4U').prop('checked', true);
                                    break;
                                case 'V':
                                    $('#d5U').prop('checked', true);
                                    break;
                                case 'S':
                                    $('#d6U').prop('checked', true);
                                    break;
                                case 'D':
                                    $('#d7U').prop('checked', true);
                                    break;
                            }
                        }                        
                        break;                                       
                }                
                                
                //OBTENERMOS LOS VALORES DE LOS DELIVERYS
                var conI;
                var dl =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS DELIVERYS
                var pl =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS PALLETS
                var pz =[]; //DECLARAMO EL ARRAY EN EL QUE VAMOS A GUARDAR LOS PIEZAS
                
                dl[1] = event.dl1;
                pl[1] = event.pl1;
                pz[1] = event.pz1;

                dl[2] = event.dl2;
                pl[2] = event.pl2;
                pz[2] = event.pz2;

                dl[3] = event.dl3;
                pl[3] = event.pl3;
                pz[3] = event.pz3;

                dl[4] = event.dl4;
                pl[4] = event.pl4;
                pz[4] = event.pz4;

                dl[5] = event.dl5;
                pl[5] = event.pl5;
                pz[5] = event.pz5;

                dl[6] = event.dl6;
                pl[6] = event.pl6;
                pz[6] = event.pz6;

                dl[7] = event.dl7;
                pl[7] = event.pl7;
                pz[7] = event.pz7;

                dl[8] = event.dl8;
                pl[8] = event.pl8;
                pz[8] = event.pz8;

                dl[9] = event.dl9;
                pl[9] = event.pl9;
                pz[9] = event.pz9;
                
                dl[10] = event.dl10;
                pl[10] = event.pl10;
                pz[10] = event.pz10;

                dl[11] = event.dl11;
                pl[11] = event.pl11;
                pz[11] = event.pz11;

                dl[12] = event.dl12;
                pl[12] = event.pl12;
                pz[12] = event.pz12;

                dl[13] = event.dl13;
                pl[13] = event.pl13;
                pz[13] = event.pz13;

                dl[14] = event.dl14;
                pl[14] = event.pl14;
                pz[14] = event.pz14;

                dl[15] = event.dl15;
                pl[15] = event.pl15;
                pz[15] = event.pz15;

                dl[16] = event.dl16;
                pl[16] = event.pl16;
                pz[16] = event.pz16;

                dl[17] = event.dl17;
                pl[17] = event.pl17;
                pz[17] = event.pz17;

                dl[18] = event.dl18;
                pl[18] = event.pl18;
                pz[18] = event.pz18;

                dl[19] = event.dl19;
                pl[19] = event.pl19;
                pz[19] = event.pz19;     

                dl[20] = event.dl20;
                pl[20] = event.pl20;
                pz[20] = event.pz20;         
                
                for (x = 1; x < 21; x++ ){  
                    //console.log(dl[x]);
                    if( dl[x] != "" && dl[x] != null ){ //EVALUAMOS SI TIENE CONTENIDO PARA MOSTRAR O NO LOS COMPONENTES
                        //console.log('#componentes'+x+'Cons show()');
                        $('#componentes'+x+'Cons').show();
                        $('#delivery'+x+'Cons').val(dl[x]);
                        $('#pallets'+x+'Cons').val(pl[x]);
                        $('#pzas'+x+'Cons').val(pz[x]);
                        $('#componentes'+x+'U').show();
                        $('#delivery'+x+'U').val(dl[x]);
                        $('#pallets'+x+'U').val(pl[x]);
                        $('#pzas'+x+'U').val(pz[x]);
                    }else{
                        //console.log('#componentes'+x+'Cons hide()');
                        $('#delivery'+x+'U').val("");
                        $('#pallets'+x+'U').val("");
                        $('#pzas'+x+'U').val(""); 
                        $('#componentes'+x+'Cons').hide(); 
                        $('#componentes'+x+'U').hide(); 
                        $('#delivery'+x+'U').val("");
                        $('#pallets'+x+'U').val("");
                        $('#pzas'+x+'U').val("");
                    }
                }
                
                //APARTADO PARA MOSTRAR EL NUMERO DE FACTURA DEL ENVIO DE ACUERDO AL ESTADO
                var tipoEstado = event.estado;
                //console.log(tipoEstado);
                if (tipoEstado == '6'){ //componentesFacura
                    $('#componentesFacura').show();
                    $('#factura').val(factura);
                }else {
                    $('#componentesFacura').hide();
                }
                
                //AHORA SI LAS VALIDACIONES DEL EVENTO PARA PODER VISUALIZAR Y MODIFICAR
                //CON LOS REPECTIVOS CAMPOS
                if (dToday > 0){ //SI ES DE LUNES A SABADO
                    console.log(dToday);
                    if (dToday < 5){
                        console.log('<5');
                        if (hToday >= 17 ){
                            //LANZA MODAL DE VISUALIZACION
                            $('#idCons').val(event.id); 
                            $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));    
                            $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                            $('#finCons').val(moment(event.end).format('HH:mm'));
                            $('#userAsigCons').val(event.userAsig);
                            $('#diaSelectCons').val(event.diasReg);
                            $('#estadoCons').val(descEstado);

                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#soldCons').val(event.shippTo);
                            $('#nombreClienteCons').val(event.cliente);
                            $('#deliveryCons').val(event.shippTo);  
                            $('#cargaCons').val(event.pallets);
                            $('#pzasCons').val(event.piezas); 
                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placaCons').val(event.placas);
                            $('#rutaCons').val(event.ruta);
                            $('#dockCons').val(event.puerta);    
                            $('#choferCons').val(event.chofer);
                            $('#modeTransportCons').val(event.modeTransport);

                            //DATOS COMPLEMENTARIOS
                            $('#notaCons').val(event.nota);

                            //DATOS DE ESTADO
                            $('#selectEvent').modal('show'); 
                        }  else {
                            if (today > event.fecha && today != event.fecha ){
                                //LANZA MODAL DE VISUALIZACION                                
                                $('#idCons').val(event.id); 
                                $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));   
                                $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                $('#finCons').val(moment(event.end).format('HH:mm'));
                                $('#userAsigCons').val(event.userAsig);
                                $('#diaSelectCons').val(event.diasReg);
                                $('#estadoCons').val(descEstado);

                                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                $('#soldCons').val(event.shippTo);
                                $('#nombreClienteCons').val(event.cliente);
                                $('#deliveryCons').val(event.shippTo);  
                                $('#cargaCons').val(event.pallets);
                                $('#pzasCons').val(event.piezas); 
                                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                $('#placaCons').val(event.placas);
                                $('#rutaCons').val(event.ruta);
                                $('#dockCons').val(event.puerta);    
                                $('#choferCons').val(event.chofer);
                                $('#modeTransportCons').val(event.modeTransport);

                                //DATOS COMPLEMENTARIOS
                                $('#notaCons').val(event.nota);

                                //DATOS DE ESTADO
                                $('#selectEvent').modal('show'); 
                            } else if (today <= event.fecha ){
                                if (today == event.fecha){
                                    if (hAntes >= 3 ){
                                        if (event.userAsig == sessionUser || event.usuario == sessionUser){
                                            $('#idEventU').val(event.id); 
                                            $('#idEventRelacion').val(event.campRelacion);
                                            $('#fechaU').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                            $('#fechaFU').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                            $('#startU').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                            $('#endU').val(moment(event.end).format('HH:mm'));
                                            $('#userAsigU').val(event.userAsig);

                                            $('#diaSelectU').val(event.diasReg);
                                            $('#estadoU').val(descEstado);

                                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                            $('#soldToU').val(event.shippTo);
                                            $('#nameClienteU').val(event.cliente);
                                            $('#deliveryU').val(event.shippTo);  
                                            $('#cargaU').val(event.pallets);
                                            $('#pzasU').val(event.piezas); 
                                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                            $('#placaU').val(event.placas);
                                            $('#rutaU').val(event.ruta);
                                            $('#dockU').val(event.puerta);    
                                            $('#choferU').val(event.chofer);
                                            $('#modeTransportU').val(event.modeTransport);

                                            //DATOS COMPLEMENTARIOS
                                            $('#notaU').val(event.nota);

                                            //LANZAMOS MODAL
                                            $('#updateEvent').modal('show');
                                        }else {
                                           $('#idCons').val(event.id); 
                                            $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                            $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));     
                                            $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                            $('#finCons').val(moment(event.end).format('HH:mm'));
                                            $('#userAsigCons').val(event.userAsig);
                                            $('#diaSelectCons').val(event.diasReg);
                                            $('#estadoCons').val(descEstado);

                                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                            $('#soldCons').val(event.shippTo);
                                            $('#nombreClienteCons').val(event.cliente);
                                            $('#deliveryCons').val(event.shippTo);  
                                            $('#cargaCons').val(event.pallets);
                                            $('#pzasCons').val(event.piezas); 
                                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                            $('#placaCons').val(event.placas);
                                            $('#rutaCons').val(event.ruta);
                                            $('#dockCons').val(event.puerta);    
                                            $('#choferCons').val(event.chofer);
                                            $('#modeTransportCons').val(event.modeTransport);

                                            //DATOS COMPLEMENTARIOS
                                            $('#notaCons').val(event.nota);

                                            //DATOS DE ESTADO
                                            $('#selectEvent').modal('show');  
                                        }                                       
                                    }else if (hAntes < 3 ){
                                        $('#idCons').val(event.id); 
                                        $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                        $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                        $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                        $('#finCons').val(moment(event.end).format('HH:mm'));
                                        $('#userAsigCons').val(event.userAsig);
                                        $('#diaSelectCons').val(event.diasReg);
                                        $('#estadoCons').val(descEstado);

                                        //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                        $('#soldCons').val(event.shippTo);
                                        $('#nombreClienteCons').val(event.cliente);
                                        $('#deliveryCons').val(event.shippTo);  
                                        $('#cargaCons').val(event.pallets);
                                        $('#pzasCons').val(event.piezas); 
                                        //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                        $('#placaCons').val(event.placas);
                                        $('#rutaCons').val(event.ruta);
                                        $('#dockCons').val(event.puerta);    
                                        $('#choferCons').val(event.chofer);
                                        $('#modeTransportCons').val(event.modeTransport);

                                        //DATOS COMPLEMENTARIOS
                                        $('#notaCons').val(event.nota);

                                        //DATOS DE ESTADO
                                        $('#selectEvent').modal('show');
                                    }
                                } else {
                                    if (event.userAsig == sessionUser || event.usuario == sessionUser ){
                                        $('#idEventU').val(event.id); 
                                        $('#idEventRelacion').val(event.campRelacion); 
                                        $('#fechaU').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                        $('#fechaFU').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                        $('#startU').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                        $('#endU').val(moment(event.end).format('HH:mm'));
                                        $('#userAsigU').val(event.userAsig);

                                        $('#diaSelectU').val(event.diasReg);
                                        $('#estadoU').val(descEstado);

                                        //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                        $('#soldToU').val(event.shippTo);
                                        $('#nameClienteU').val(event.cliente);
                                        $('#deliveryU').val(event.shippTo);  
                                        $('#cargaU').val(event.pallets);
                                        $('#pzasU').val(event.piezas); 
                                        //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                        $('#placaU').val(event.placas);
                                        $('#rutaU').val(event.ruta);
                                        $('#dockU').val(event.puerta);    
                                        $('#choferU').val(event.chofer);
                                        $('#modeTransportU').val(event.modeTransport);

                                        //DATOS COMPLEMENTARIOS
                                        $('#notaU').val(event.nota);

                                        //LANZAMOS MODAL
                                        $('#updateEvent').modal('show');
                                    }else{
                                        $('#idCons').val(event.id); 
                                        $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                        $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));     
                                        $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                        $('#finCons').val(moment(event.end).format('HH:mm'));
                                        $('#userAsigCons').val(event.userAsig);
                                        $('#diaSelectCons').val(event.diasReg);
                                        $('#estadoCons').val(descEstado);

                                        //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                        $('#soldCons').val(event.shippTo);
                                        $('#nombreClienteCons').val(event.cliente);
                                        $('#deliveryCons').val(event.shippTo);  
                                        $('#cargaCons').val(event.pallets);
                                        $('#pzasCons').val(event.piezas); 
                                        //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                        $('#placaCons').val(event.placas);
                                        $('#rutaCons').val(event.ruta);
                                        $('#dockCons').val(event.puerta);    
                                        $('#choferCons').val(event.chofer);
                                        $('#modeTransportCons').val(event.modeTransport);

                                        //DATOS COMPLEMENTARIOS
                                        $('#notaCons').val(event.nota);

                                        //DATOS DE ESTADO
                                        $('#selectEvent').modal('show'); 
                                    }
                                }
                            }
                        }                      
                    } else {
                        if (dToday == 5 && hToday >= 19){
                            console.log("4. today: "+ today + "eInicioReg: "+event.inicioReg+ "userAsig: "+event.userAsig+" userAct: "+ sessionUser );
                            //LANZA MODAL DE VISUALIZACION
                            $('#idCons').val(event.id); 
                            $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));      
                            $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                            $('#finCons').val(moment(event.end).format('HH:mm'));
                            $('#userAsigCons').val(event.userAsig);
                            $('#diaSelectCons').val(event.diasReg);
                            $('#estadoCons').val(descEstado);

                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#soldCons').val(event.shippTo);
                            $('#nombreClienteCons').val(event.cliente);
                            $('#deliveryCons').val(event.shippTo);  
                            $('#cargaCons').val(event.pallets);
                            $('#pzasCons').val(event.piezas); 
                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placaCons').val(event.placas);
                            $('#rutaCons').val(event.ruta);
                            $('#dockCons').val(event.puerta);    
                            $('#choferCons').val(event.chofer);
                            $('#modeTransportCons').val(event.modeTransport);

                            //DATOS COMPLEMENTARIOS
                            $('#notaCons').val(event.nota);

                            //DATOS DE ESTADO
                            $('#selectEvent').modal('show');                        
                        } else {
                            if (today > event.inicioReg && today != event.inicioReg ){
                                console.log("5. today: "+ today + "eInicioReg: "+event.inicioReg+ "userAsig: "+event.userAsig+" userAct: "+ sessionUser );
                                //LANZA MODAL DE VISUALIZACION
                                $('#idCons').val(event.id); 
                                $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                $('#finCons').val(moment(event.end).format('HH:mm'));
                                $('#userAsigCons').val(event.userAsig);
                                $('#diaSelectCons').val(event.diasReg);
                                $('#estadoCons').val(descEstado);

                                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                $('#soldCons').val(event.shippTo);
                                $('#nombreClienteCons').val(event.cliente);
                                $('#deliveryCons').val(event.shippTo);  
                                $('#cargaCons').val(event.pallets);
                                $('#pzasCons').val(event.piezas); 
                                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                $('#placaCons').val(event.placas);
                                $('#rutaCons').val(event.ruta);
                                $('#dockCons').val(event.puerta);    
                                $('#choferCons').val(event.chofer);
                                $('#modeTransportCons').val(event.modeTransport);

                                //DATOS COMPLEMENTARIOS
                                $('#notaCons').val(event.nota);

                                //DATOS DE ESTADO
                                $('#selectEvent').modal('show'); 
                            } else if (today <= event.inicioReg){
                                if (event.userAsig == sessionUser || event.usuario == sessionUser){    
                                    $('#idEventU').val(event.id); 
                                    $('#idEventRelacion').val(event.campRelacion);
                                    $('#fechaU').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                    $('#fechaFU').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                    $('#startU').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                    $('#endU').val(moment(event.end).format('HH:mm'));
                                    $('#userAsigU').val(event.userAsig);

                                    $('#diaSelectU').val(event.diasReg);
                                    $('#estadoU').val(descEstado);

                                    //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                    $('#soldToU').val(event.shippTo);
                                    $('#nameClienteU').val(event.cliente);
                                    $('#deliveryU').val(event.shippTo);  
                                    $('#cargaU').val(event.pallets);
                                    $('#pzasU').val(event.piezas); 
                                    //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                    $('#placaU').val(event.placas);
                                    $('#rutaU').val(event.ruta);
                                    $('#dockU').val(event.puerta);    
                                    $('#choferU').val(event.chofer);
                                    $('#modeTransportU').val(event.modeTransport);

                                    //DATOS COMPLEMENTARIOS
                                    $('#notaU').val(event.nota);

                                    //LANZAMOS MODAL
                                    $('#updateEvent').modal('show');
                                }
                            }
                        }
                        if (dToday == 6 && hToday >= 12){
                            //LANZA MODAL DE VISUALIZACION
                            $('#idCons').val(event.id); 
                            $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));     
                            $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                            $('#finCons').val(moment(event.end).format('HH:mm'));
                            $('#userAsigCons').val(event.userAsig);
                            $('#diaSelectCons').val(event.diasReg);
                            $('#estadoCons').val(descEstado);

                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#soldCons').val(event.shippTo);
                            $('#nombreClienteCons').val(event.cliente);
                            $('#deliveryCons').val(event.shippTo);  
                            $('#cargaCons').val(event.pallets);
                            $('#pzasCons').val(event.piezas); 
                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placaCons').val(event.placas);
                            $('#rutaCons').val(event.ruta);
                            $('#dockCons').val(event.puerta);    
                            $('#choferCons').val(event.chofer);
                            $('#modeTransportCons').val(event.modeTransport);

                            //DATOS COMPLEMENTARIOS
                            $('#notaCons').val(event.nota);

                            //DATOS DE ESTADO
                            $('#selectEvent').modal('show'); 
                        }  else{
                            if (event.userAsig == sessionUser || event.usuario == sessionUser){
                                $('#idEventU').val(event.id); 
                                $('#idEventRelacion').val(event.campRelacion);
                                $('#fechaU').val(moment(event.fecha).format('DD/MM/YYYY'));    
                                $('#fechaFU').val(moment(event.finReg).format('DD/MM/YYYY'));      
                                $('#startU').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                                $('#endU').val(moment(event.end).format('HH:mm'));
                                $('#userAsigU').val(event.userAsig);
                                //$('#periodoTiempoU').val("1");

                                $('#diaSelectU').val(event.diasReg);
                                $('#estadoU').val(descEstado);

                                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                                $('#soldToU').val(event.shippTo);
                                $('#nameClienteU').val(event.cliente);
                                $('#deliveryU').val(event.shippTo);  
                                $('#cargaU').val(event.pallets);
                                $('#pzasU').val(event.piezas); 
                                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                                $('#placaU').val(event.placas);
                                $('#rutaU').val(event.ruta);
                                $('#dockU').val(event.puerta);    
                                $('#choferU').val(event.chofer);
                                $('#modeTransportU').val(event.modeTransport);

                                //DATOS COMPLEMENTARIOS
                                $('#notaU').val(event.nota); 

                                //LANZAMOS MODAL
                                $('#updateEvent').modal('show');
                            }
                        }
                    }
                }else { //SI ES DOMINGO SOLO DEJA VISUALIZAR
                    //LANZA MODAL DE VISUALIZACION
                    //alert(event.tipoRegistro);
                    $('#idCons').val(event.id); 
                    $('#fechaICons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                    $('#fechaFCons').val(moment(event.finReg).format('DD/MM/YYYY'));      
                    $('#inicioCons').val(moment(event.start).format('HH:mm')); //NO MOVER EL FORMATO SI NO DA HORAS INEXACTAS
                    $('#finCons').val(moment(event.end).format('HH:mm'));
                    $('#userAsigCons').val(event.userAsig);
                    $('#diaSelectCons').val(event.diasReg);
                    $('#estadoCons').val(descEstado);

                    //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                    $('#soldCons').val(event.shippTo);
                    $('#nombreClienteCons').val(event.cliente);
                    $('#deliveryCons').val(event.shippTo);  
                    $('#cargaCons').val(event.pallets);
                    $('#pzasCons').val(event.piezas); 
                    //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                    $('#placaCons').val(event.placas);
                    $('#rutaCons').val(event.ruta);
                    $('#dockCons').val(event.puerta);    
                    $('#choferCons').val(event.chofer);
                    $('#modeTransportCons').val(event.modeTransport);

                    //DATOS COMPLEMENTARIOS
                    $('#notaCons').val(event.nota);

                    //DATOS DE ESTADO
                    $('#selectEvent').modal('show'); 
                }
            },
        });
        
    //FUNCION PARA ACTUALIZAR LOS EVENTO CADA MINUTO
    var refreshId = setInterval(function() {
                    calendar.fullCalendar('refetchEvents');
                }, 50000);
    $.ajaxSetup({ cache: false });        
        
    //FUNCIONES PARA LOS MODALS HACER LOS QUERYS 
    //FUNCION PARA AGREGAR NUEVO REGISTRO A LA BASE DE DATOS
    $( "#nuevoEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/iEvento.php",
                data: parametros,                
                success: function(datos){
                    $("#datos_ajax_event").html(datos);
                    calendar.fullCalendar('refetchEvents');
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#modificarEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/uEvento.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    calendar.fullCalendar('refetchEvents');
                },
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });  
     
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/cLogin.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    $("#datos_ajax").html(datos);
                    calendar.fullCalendar('refetchEvents');
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#exit" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./extras/exit.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    calendar.fullCalendar('refetchEvents');
                    location.href="../shipp.php";
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });    
    
    //ACTUALIZA LOS DATOS EN LA BD SE MANDA EL QUERY UPDATE
    $( "#actualidarDatosUser" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./extras/uUserUser.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    load();
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });    
    
    function valida_envia(){
        var dToday = new Date().getDay();
        var h = new Date().getHours();
        if (dToday > 0 && dToday < 6 && h < 17){                    
            $('#newEvent').modal('show');
        }else {
            $('#warningHora').modal('show');
        }
    }

});  