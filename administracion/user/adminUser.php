<?php 
    session_start();    
    if( !isset($_SESSION['user']) && $_SESSION['tipo'] != 1 ){   
        echo 'Lo siento :( , debes registrarte';
        echo '<script>location.href = "../../index.php";</script>';        
    } else {
?>
<HTML>
    <LINK REL=StyleSheet HREF="../../css/estiloAdminUsuarios.css" TYPE="text/css" MEDIA=screen>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">  
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">    
    <?php     
        
        include './mInsertUser.php';
        include './mUpdateUser.php';
        include './mDeleteUser.php';
        
        //MODALS PARA SHIPP TO
        include './modals/mInsertShipp.php';
        include './modals/mUpdateShipp.php';
        include './modals/mDeleteShipp.php';
        
        $serverName = "SGLERSQL01\sqlexpress, 1433"; 
        $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
        $conn = sqlsrv_connect($serverName, $options);

        if( $conn === false ) {
            die( print_r( sqlsrv_errors(), true));
        }
        
        $sqlUsuarios = "SELECT * FROM usuarios WHERE nombre NOT LIKE ('%DESARROLLO%') ORDER BY usuario";
        $stmt = sqlsrv_query( $conn, $sqlUsuarios );
        if( $stmt === false) {
            die( print_r( sqlsrv_errors(), true) );
        }

        $sqlSoldTo = "SELECT * FROM clientes ORDER BY id";
        $stmtSold = sqlsrv_query( $conn, $sqlSoldTo );
        if( $stmtSold === false) {
            die( print_r( sqlsrv_errors(), true) );
        }
        
    ?>
    <head>        
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'>                
            </div>            
            <h3 class="tituloAdmin">                  
                ADMINISTRACI&Oacute;N
            </h3>

            <div class="fila1">  
                <button data-target="#panelAdmin" type="submit" class="btn btn-primary btn-sm pull-right glyphicon glyphicon-menu-left" style="float: right; position: absolute; top: 4%; left: 2%; width: 7.6%;" onclick = "location='../../index.php'"
                        onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'"> Regresar</button>
            </div> 
        </a>  
    </head>
    <BODY>
        <div class="contenidoPanel">  
            <br>
            <br>
            <ul class="nav nav-tabs" >
                <li class="active"><a data-toggle="tab" href="#usuarios">Usuarios</a></li>
                <li ><a data-toggle="tab" href="#soldTo">Ship To</a></li>
            </ul>
            
            <div class="tab-content">   
                <div id="usuarios" class="tab-pane fade in active" style="height: 85%">
                    <div class='col-xs-6'>
                        <h3 class='text-right'>		
                            <button type="button" class="btn btn-sm btn-warning btnAddUser" data-toggle="modal" data-target="#dataRegister"><i class='glyphicon glyphicon-plus'></i> Agregar</button>
                        </h3>
                    </div>	

                    <div class="tableUsers">
                        <table class="table table-striped table-bordered table-hover">
                            <thead style="font-size:14px">
                                <th width='10%'> Usuario</th>
                                <th>Nombre</th>
                                <th width='10%'>T&eacute;lefono</th>
                                <th width='17%'>Tipo</th>
                                <th width='16%'>Acciones</th>
                            </thead>
                            <tbody style="font-size:12px">
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
                                ?>
                                    <tr>
                                        <td width='10%'><?php echo $row['usuario'];?></td>
                                        <td ><?php echo $row['nombre'];?></td>
                                        <td width='10%'><?php echo $row['telefono'];?></td>
                                        <?php 
                                            switch ($row['tipo']){
                                                case 0:                                    
                                                    $rowt = "Desactivado";
                                                break;
                                                case 1:  
                                                    $rowt = "Almacen";
                                                    
                                                break;
                                                case 2:                                    
                                                    $rowt = "Customer Service";
                                                break;
                                                default:
                                                    $rowt = 'Error < adminUsuarios class: adminUser at line 86 >';
                                                break;
                                            }
                                        ?>

                                        <td width='17%'><?php echo $rowt ?></td>
                                        <td width='16%'>                        
                                            <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#dataUpdate" data-usuario="<?php echo $row['usuario']?>" data-nombre="<?php echo $row['nombre']?>" data-telefono="<?php echo $row['telefono']?>" data-tipo="<?php echo $row['tipo']?>" data-contrasena="<?php echo $row['contrasena']?>" data-pass="<?php echo $row['contrasena']?>" ><i class='glyphicon glyphicon-edit'></i> Modificar</button>
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#dataDelete" data-usuario="<?php echo $row['usuario']?>" data-nombre="<?php echo $row['nombre']?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                                        </td>
                                    </tr>                 
                               <?php 
                                    }
                               ?>
                            </tbody>
                        </table>            
                    </div>
                </div>
                <div id="soldTo" class="tab-pane fade " style="height: 85%">
                    <div class='col-xs-6'>
                        <h3 class='text-right'>		
                            <button type="button" class="btn btn-sm btn-warning btnAddUser" data-toggle="modal" data-target="#iShipp"><i class='glyphicon glyphicon-plus'></i> Agregar</button>
                        </h3>
                    </div>	

                    <div class="tableShipp">
                        <table class="table table-striped table-bordered table-hover">
                            <thead style="font-size:14px">
                                <th width='10%'> Ship To</th>
                                <th>Nombre</th>
                                <th width='16%'>Acciones</th>
                            </thead>
                            <tbody style="font-size:12px">
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $stmtSold, SQLSRV_FETCH_ASSOC) ) {
                                ?>
                                    <tr>
                                        <td width='10%'><?php echo $row['soldToParty'];?></td>
                                        <td ><?php echo $row['nombre'];?></td>                                        
                                        <td width='16%'>                        
                                            <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#uShipp" data-shipp="<?php echo $row['soldToParty']?>" data-nombre="<?php echo $row['nombre']?>" ><i class='glyphicon glyphicon-edit'></i> Modificar</button>
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#dShipp" data-shipp="<?php echo $row['soldToParty']?>" data-nombre="<?php echo $row['nombre']?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                                        </td>
                                    </tr>                 
                               <?php 
                                    }
                               ?>
                            </tbody>
                        </table>            
                    </div>
                </div>
                    
            </div>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            <script src="funcionesAdmin.js"></script>
        </div>   
    </BODY>
    <?php 
        }
    ?>