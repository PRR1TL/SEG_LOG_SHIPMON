<form id="actualidarDatos">
    <div class="modal fade" id="dataUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Modificar Usuario:</h4>
                </div>
                <div class="modal-body">
                    <div id="datos_ajax"></div>
                    <input type="hidden" class="form-control" id="usuario" name="usuario" maxlength="6" required>
                    <!--</div>-->
                    <div class="form-group">
                        <label for="nombre0" class="control-label">Nombre:</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" required>
                    </div>
                    <div class="form-group">
                        <label for="moneda0" class="control-label">Número Telefónico:</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" onkeypress="return permite(event,'num')" maxlength="10">
                    </div>
                    <div class="form-group">
                        <label for="tipo0" class="control-label">Puesto:</label> 
                        <select type ="text" class="form-control" id="tipo" name="tipo"required >                    
                            <option value="" selected disabled> Selecciona Puesto </option>
                            <option value="1"> Almacen / Supervisor </option>
                            <option value="2"> Customer Service </option>  
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pass1" class="control-label">Contraseña</label>
                        <input type="password" class="form-control" id="contrasena" name="contrasena" style="text-transform:uppercase;" minlength="6" maxlength="30" required> 
                    </div>            
                    <div class="form-group">
                        <label for="pass2" class="control-label">Verificación de Contraseña:</label>
                        <input type="password" class="form-control" id="contrasena2" name="contrasena2" style="text-transform:uppercase;" minlength="6" maxlength="30" required> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>
