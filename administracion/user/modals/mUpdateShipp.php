<form id="actualizarShipp">
    <div class="modal fade" id="uShipp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Modificar Shipp To:</h4>
                </div>
                
                <div class="modal-body">                    
                    <div id="datos_ajax"></div>
                    <input type="hidden" class="form-control" id="idShippTo" name="idShippTo" required>
                    <!--</div>-->
                    <div class="form-group">
                        <label for="nombre0" class="control-label">Shipp To:</label>
                        <input type="text" class="form-control" id="shippToU" name="shippToU" onkeypress="return permite(event,'num')" minlength="8" maxlength="12" readonly >
                    </div>
                    <div class="form-group">
                        <label for="moneda0" class="control-label">Nombre:</label>
                        <input type="text" class="form-control" id="nameShippU" name="nameShippU" onkeypress="return permite(event,'car')" maxlength="49">
                    </div>                    
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>