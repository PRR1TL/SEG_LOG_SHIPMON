<form id="nuevoShipp">
    <div class="modal fade" id="iShipp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Agregar Shipp To</h4>
        </div>
        <div class="modal-body">
            <div id="datos_ajax"></div>
            <div class="form-group">
                <label for="nombre0" class="control-label">Shipp To:</label>
                <input type="text" class="form-control" id="shippTo" name="shippTo" onkeypress="return permite(event,'num')" minlength="8" maxlength="12" >
            </div>
            <div class="form-group">
                <label for="moneda0" class="control-label">Nombre:</label>
                <input type="text" class="form-control" id="nameShipp" name="nameShipp" onkeypress="return permite(event,'car')" maxlength="49">
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Guardar datos</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
    </div>
    </div>
</form>