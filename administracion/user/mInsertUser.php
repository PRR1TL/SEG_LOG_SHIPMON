<form id="guardarDatos">
    <div class="modal fade" id="dataRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Agregar Usuario</h4>
        </div>
        <div class="modal-body">
            
	<div id="datos_ajax_register"></div>
            <div class="form-group">
                <label for="codigo0" class="control-label">Usuario:</label>
                <input type="text" class="form-control" id="codigo0" name="usuario" placeholder="PRR1TL" required maxlength="6">
            </div>
            <div class="form-group">
                <label for="nombre0" class="control-label">Nombre:</label>
                <input type="text" class="form-control" id="nombre0" name="nombre" placeholder="Areli Perez Calixto" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" required>
            </div>
    	    <div class="form-group">
                <label for="moneda0" class="control-label">Número Telefónico:</label>
                <input type="text" class="form-control" id="moneda0" name="telefono" placeholder="7221234567" onkeypress="return permite(event,'num')" maxlength="10">
            </div>
            <div class="form-group">
                <label for="tipo0" class="control-label">Puesto:</label>
                <select type ="text" class="form-control" id="tipo0" name="tipo"required >                    
                    <option value="" selected disabled> Selecciona Puesto </option>
                    <option value="1"> Almacen / Supervisor </option>
                    <option value="2"> Customer Service </option>                    
                </select>
            </div>
            <div class="form-group">
                <label for="continente0" class="control-label">Contraseña</label>
                <input type="password" class="form-control" id="passwd" name="contrasena" style="text-transform:uppercase;" minlength="6" maxlength="35" required>
            </div>            
            <div class="form-group">
                <label for="capital0" class="control-label">Verificación de Contraseña:</label>
                <input type="password" class="form-control" id="passwd2" name="contrasena2" style="text-transform:uppercase;" minlength="6" maxlength="30" required> 
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Guardar datos</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
    </div>
    </div>
</form>